<?php
require_once '../dao/AlunoDao.php'; //importando a classe alunoDAO
require_once '../model/Aluno.php'; //importando a classe ALUNO
session_start();// iniciando a sessão

$prof_cpf = $_SESSION['cpf']; // armazenando o cpf da sessão numa variável local

if(!(isset($prof_cpf) and isset($_POST))){ // verificando se o cpf do professor está setado e se os dados dos alunos foram passados por post
    echo '<h1>Professor nao logado</h1>'; 
}
$al = filter_input_array(INPUT_POST, FILTER_DEFAULT); // filtrando os dados dos alunos passados por POST



$a = new AlunoDao(); //criando um objeto do tipo aluno

//verificando se o cpf ja foi cadastrado
//$res = $a->getAlunoFull($prof_cpf, $al['cpf']);

//if($res != null){//verificando se a resposta do $res é diferente de nulo
//    //CPF já Cadastrado
//    header("location:".WEB."/cadastrarAluno.php?2");
//    die(); //fim da execução
//}


//armazenando os dados dos alunos recebidos por POST em variaveis
$nome = $al['nome'];
$grupo = $al['grupo']; 
$sexo = $al['sexo']; 
$altura = $al['altura']; 
$peso = $al['peso'];
$idade = $al['idade'];
$cintura = $al['cintura'];
$quadril= $al['quadril']; 
$envergadura = $al['envergadura']; 
$flexibilidade = $al['flexibilidade']; 
$resistencia_ab = $al['resistencia_abdominal'];
$velocidade = $al['velocidade'];
$salto = $al['salto_distancia'];
$triceps = $al['triceps'];
$subscapular = $al['subscapular'];
$resistencia_cv = $al['resistencia_cardiovascular'];
$arremesso = $al['arremesso_ball'];
$agilidade = $al['agilidade'];



$aluno = new Aluno( // criando o objeto do aluno apartir desses dados obtidos
        $nome,
        //$cpf,
        $idade,
        $grupo,
        $sexo,
        "",
        $altura,
        $peso,
        $cintura,
        $quadril,
        $envergadura,
        $flexibilidade,
        $resistencia_ab,
        $velocidade,
        $salto,
        $triceps,
        $subscapular,
        $resistencia_cv,
        $arremesso,
        $agilidade
        );
$a->cadastrarAluno($aluno, $prof_cpf); // cadastrando o aluno junto com o cpf do professor
header("location:".WEB."/cadastrarAluno.php?1"); // redirenciando para a página do aluno