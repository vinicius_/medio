<?php
include '../include/Config.php'; //incluindo as configurações
require '../dao/ProfessorDao.php'; // importando a classe professorDAO

if(!(isset($_POST['cpf']) and ($_POST['senha']))){
    echo 'Missing posts';
    die();
}

$post = filter_input_array(INPUT_POST, FILTER_DEFAULT); // Pegando os dados passados por POST e filtrando

$cpf = $post['cpf']; // pegando CPF do POST
$senha = $post['senha']; // Pegando a senha do POST

$p = new ProfessorDao(); //Criando o objeto do tipo ProfessorDAO
$p->buscarProfessor($cpf, $senha); // buscando no banco o professor correspondente com o cpf e senha pegos pelo POST
$res = $p->getResultado(); // Capturando o resultado
//print_r($res);
if($res != null){ // se o resultado estiver vazio então o professor nao foi encontrado
    session_start(); //iniciando a sessão
    $_SESSION['cpf'] = $res[0]['cpf']; // armazenando o cpf do professor na sessão
    $_SESSION['professor'] =  serialize(new Professor($res[0]['nome'], $res[0]['cpf'], $res[0]['telefone'],$res[0]['email'],$res[0]['senha'])); //Armazenando o objeto do professor na sessão
    header("location:".WEB."/professor.php"); //redirecionando a página para professor.php
}else{
    header("location:".WEB."/login.php?1"); //redirecionando para pagina login caso o professor não for encontrado
}

