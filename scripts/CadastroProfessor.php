<?php
require_once '../dao/ProfessorDao.php'; //importando a classe ProfessorDAO

$array = filter_input_array(INPUT_POST, FILTER_DEFAULT); // filtrando os dados do professor passados por POST

//armazenando os dados do professor em variaveis locais
$nomeProfessor = $array['nome']; 
$cpfProfessor = $array['cpf'];
$telefoneProfessor = $array['telefone'];
$emailProfessor = $array['email'];
$senhaProfessor = $array['senha'];

session_start(); //inicando a sessão
$_SESSION['cpf'] = null;// definindo o cpf como nulo para depois muda-lo

try {
    $c = new ProfessorDao(); //criando o objeto do tipo professorDAO     
    $p = new Professor($nomeProfessor, $cpfProfessor, $telefoneProfessor, $emailProfessor, $senhaProfessor); //criando o objeto do tipo professor com base nos dados passados por POST
    $c->cadastrarProfessor($p); //cadastrando o professor
    
} catch (Exception $exc) {
    echo 'ERROR: '.$exc->getTraceAsString();
}

$_SESSION['cpf'] = $cpfProfessor; // armazenando o CPF do professor na sessão 
$_SESSION['professor'] =  serialize($p); // serializando o professor antes de inserilo na sessão
header("location:".WEB."/professor.php"); //redirenciando para a página do professor
