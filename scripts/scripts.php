<?php

function períodoDia() {
    date_default_timezone_set('America/Sao_Paulo');
    $hora = date('H:i:s');
    if($hora >= date('H:i:s', strtotime('00:00:00')) && $hora < date('H:i:s', strtotime('06:00:00'))) {
        return 'Boa noite';
    } else  if($hora >= date('H:i:s', strtotime('06:00:00')) && $hora < date('H:i:s', strtotime('12:00:00'))) {
        return 'Bom dia';
    } else  if($hora >= date('H:i:s', strtotime('12:00:00')) && $hora < date('H:i:s', strtotime('18:00:00'))) {
        return 'Boa tarde';

    } else  if($hora >= date('H:i:s', strtotime('18:00:00')) && $hora <= date('H:i:s', strtotime('23:59:59'))) {

        return 'Boa noite';
    }
}

function nivel($w) {
    if($w == "Excelente") {
        return 100;
    }else if($w == "Muito Bom"){
        return 80;
    }else if($w == "Bom"){
        return 60;
    }else if($w == "Razoavel"){
        return 40;
    }else if($w == "Fraco"){
        return 20;
    }
}

function barraBg($w) {
    if($w == "Excelente") {
        return "bg-success";
    }else if($w == "Muito Bom"){
        return "bg-success";
    }else if($w == "Bom"){
        return "bg-success";
    }else if($w == "Razoavel"){
        return "bg-danger";
    }else if($w == "Fraco"){
        return "bg-danger";
    }
}