<!DOCTYPE html>
<html lang="ptbr">

  <head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <meta name="description" content="">
    
    <meta name="author" content="">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <link rel="stylesheet" href="web/css/indexstyle.css">
    <link rel="stylesheet" href="web/css/generalstyle.css">
  </head>

  <body id="page-top">

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="mainNav">
          <div class="container">
            <a class="navbar-brand js-scroll-trigger" id="logo" href="#page-top">SCADAF</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
              <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="#">Sobre nós</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="web/login.php">Login</a>
                </li>
              </ul>
            </div>
          </div>
        </nav>

    <header class="bg-dark text-white">
      <div class="container text-center">
        <h1>Bem vindo(a) ao SCADAF</h1>
        <p>Uma plataforma onde você pode gerenciar as suas aulas de Educação Física.</p>
        <a href="web/login.php" class="btn btn-light btn-lg" style="padding: 1em;">Começar agora!</a>
      </div>
    </header>

    <section id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <h2>Controle seus alunos com facilidade</h2>
            <p class="lead">Com o SCADAF você terá um controle muito mais intuitivo para os seus alunos, pois</p>
            <ul>
              <li>o sistema cadastra os alunos e os classificam de acordo com as suas capacidades físicas;</li>
              <li>ao aplicar filtros você pode ver quais alunos podem praticar determinados esportes;</li>
              <li>terá o controle individual do aluno bem como um área destinada para cada esporte com a lista de todos os alunos aptos para praticá-los.</li>
            </ul>
          </div>
        </div>
      </div>
    </section>

    <section id="services" class="bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <h2>Análise simples</h2>
            <p class="lead">Com esse sistema você poderá analisar de forma intuitiva o desempenho dos alunos, tanto coletivamente, quanto individualmente.</p>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
      </div>
      <!-- /.container -->
    </footer>
  </body>

</html>