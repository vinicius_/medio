<?php
/**
 * A classe Esporte é responsável atribuir a classificação do aluno à cada esporte com base nos testes feitos na classe Classificação
 */
class Esporte {
    private $classify; //variável que armazenará a classificação do aluno em cada modalidade
    private $sify; //essa variável é a transformação em array do objeto classify
    
    public $atletismo;
    public $futebol;
    public $volei;
    public $basquete;
    public $handbol;
    
    public function __construct(Classificacao $classify, $tipo) {
        $this->classify = $classify;
        $this->preparar();//transformando o objeto classify em array
        
        /**
         * Existem duas formas de classificação: IP (importante) e IM (imprescindivel)
         * A forma de classificação IM é mais rigorosa que IP, através da variável $tipo é possivel informar 
         * qual o tipo de classificação será executado
         */
        if($tipo == 1){
            $this->atletismoIP();
            $this->futebolIP();
            $this->voleiIP();
            $this->basqueteIP();
            
        }else{
            $this->atletismoIM();
            $this->futebolIM();
            $this->voleiIM();
            $this->basqueteIM();
            
        }
    }
    
    /**
     * Metodo que transforma o objeto do tipo classificação em array
     */
    private function preparar(){
        $this->sify = array(
            "flexibilidade" => $this->classify->getFlexibilidade(), //flexibilidade
            "forca_explosiva_i" => $this->classify->getSalto(),     //força explosiva menbros inferiores
            "resistencia_m_l" => $this->classify->getResistencia_ab(), //resistencia muscular localizada
            "resistencia_a" => $this->classify->getResistencia_anaerobica(), //resistencia aerobica[cardiovascular]
            "velocidade" => $this->classify->getCorrida(), // velocidade
            "agilidade" => $this->classify->getAgilidade(), // agilidade
            "forca_explosiva_s" => $this->classify->getArremesso_ball() //força explosiva membros superiores
            );
    }
    /**
     * Comparação da classificicação do atletismo com os parâmetros do atletismo
     */
    private function atletismoIP(){
        //iP
        //flexibilidade
        //força explosiva membros inferiores (salto)
        //resistencia muscular localizada (resistencia abdominal)
        //resistencia anaerobica
        //pra passar pra atletismo precisa de 2 ao menos
        $quant = 0;
        
        if ($this->sify['flexibilidade'] == "Muito Bom" or $this->sify['flexibilidade'] == "Excelente") {
            $quant++;
        }
        if ($this->sify['forca_explosiva_i'] == "Muito Bom" or $this->sify['forca_explosiva_i'] == "Excelente") {
            $quant++;
        }
        if ($this->sify['resistencia_m_l'] == "Muito Bom" or $this->sify['resistencia_m_l'] == "Excelente") {
            $quant++;
        }
        if ($this->sify['resistencia_a'] == "Muito Bom" or $this->sify['resistencia_a'] == "Excelente") {
            $quant++;
        }
        
        
        if($quant >= 2){
            $this->atletismo = true;
        }else{
            $this->atletismo = false;
        }        
    }
    /**
     * Comparação da classificicação do atletismo com os parâmetros do atletismo
     */
    private function atletismoIM(){
        
        if($this->sify['velocidade'] == "Excelente"){
            $this->atletismo = true;
        }else{
            $this->atletismo = false;
        }
    }

    /**
     * Comparação da classificicação do futebol com os parâmetros do futebol
     */
    private function futebolIP(){
        ///resistencia muscular localizada (porem é generalizada e nao tronco)
        //velocidade é a fusão de velocidade de movimento e de reação
        $quant = 0;
        
        if ($this->sify['resistencia_m_l'] == "Muito Bom" or $this->sify['resistencia_m_l']  == "Excelente") {
            $quant++;
        }
        if ($this->sify['resistencia_a'] == "Muito Bom" or $this->sify['resistencia_a'] == "Excelente") {
            $quant++;
        }
        if ($this->sify['velocidade'] == "Muito Bom"or $this->sify['velocidade'] == "Excelente") {
            $quant++;
        }
        if ($this->sify['agilidade'] == "Muito Bom" or $this->sify['agilidade'] == "Excelente") {
            $quant++;
        }
        
        if($quant >= 2){
            $this->futebol = true;
        }else{
            $this->futebol = false;
            
        }
        
    }
    /**
     * Comparação da classificicação do futebol com os parâmetros do futebol
     */
    private function futebolIM(){
        $quant = 0;
        if ($this->sify['forca_explosiva_i'] == "Excelente") {
            $quant++;
        }
        if ($this->sify['resistencia_a'] == "Excelente") {
            $quant++;
        }
        
        if($quant >= 1){
            $this->futebol = true;
        }else{
            $this->futebol = false;
        }
    }
    /**
     * Comparação da classificicação do volei com os parâmetros do volei
     */
    private function voleiIP(){
        $quant = 0;
        if ($this->sify['forca_explosiva_s'] == "Muito Bom" or $this->sify['forca_explosiva_s'] == "Excelente") {
            $quant++;
        }
        if ($this->sify['resistencia_m_l'] == "Muito Bom" or $this->sify['resistencia_m_l']  == "Excelente") {
            $quant++;
        }
        if ($this->sify['resistencia_a'] == "Muito Bom" or $this->sify['resistencia_a'] == "Excelente") {
            $quant++;
        }
        if ($this->sify['velocidade'] == "Muito Bom" or $this->sify['velocidade'] == "Excelente") {
            $quant++;
        }
        if ($this->sify['agilidade'] == "Muito Bom" or $this->sify['agilidade'] == "Excelente") {
            $quant++;
        }
        if($quant >= 3){
            $this->volei = true;
        }else {
            $this->volei = false;
        }
    }
    /**
     * Comparação da classificicação do volei com os parâmetros do volei
     */
    private function voleiIM() {
        if ($this->sify['forca_explosiva_i'] == "Excelente") {
            $this->volei = true;
        }else{
            $this->volei = false;
        }
    }
    /**
     * Comparação da classificicação do basquete com os parâmetros do basquete
     */
    private function basqueteIP(){
        $quant = 0;
        if ($this->sify['resistencia_m_l'] == "Muito Bom" or $this->sify['resistencia_m_l'] == "Excelente") {
            $quant++;
        }
        if ($this->sify['resistencia_a'] == "Muito Bom" or $this->sify['resistencia_a'] == "Excelente") {
            $quant++;
        }
        if ($this->sify['velocidade'] == "Muito Bom" or $this->sify['velocidade'] == "Excelente") {
            $quant++;
        }
        if ($this->sify['agilidade'] == "Muito Bom" or $this->sify['agilidade'] == "Excelente") {
            $quant++;
        }
        
        if($quant >= 2){
            $this->basquete = true;
            $this->handbol = true;
        }else {
            $this->handbol = false;
            $this->basquete = false;
        }
    }
    /**
     * Comparação da classificicação do basquete com os parâmetros do basquete
     */
    private function basqueteIM(){
        $quant = 0;
        if ($this->sify['resistencia_a'] == "Excelente") {
            $quant++;
        }
        if ($this->sify['forca_explosiva_i'] == "Excelente") {
            $quant++;
        }
        
        if($quant >= 1){
            $this->basquete = true;
            $this->handbol = true;
        }else {
            $this->basquete = false;
            $this->handbol = false;
        }
    }
}
