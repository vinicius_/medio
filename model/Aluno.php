<?php
/**
 * Classe que representará o aluno com todos os dados
 */
class Aluno {
    public $nome;
    public $idAluno;
    public $grupo;
    public $sexo;
    public $altura;
    public $peso;
    public $idade;//data nascimento
    public $cintura;
    public $quadril;
    public $envergadura;
    public $flexibilidade;
    public $resistencia_abdominal;
    public $velocidade;
    public $salto_em_discancia;
    public $triceps;
    public $subscapular;
    public $resistencia_cardiovascular;
    public $arremesso_de_medicine_ball;
    public $agilidade;
            
    
    //                  ($nome,$idAluno ="", $grupo,$sexo, $altura="", $peso="", $idade="", $cintura="", $quadril="", $envergadura="", $flexibilidade="", $resistencia_abdominal="", $velocidade="", $salto_em_discancia="", $triceps="", $subscapular="", $resistencia_cardiovascular="", $arremesso_de_medicine_ball="", $agilidade="") 
    function __construct($nome, $idade,$grupo, $sexo, $idAluno = "", $altura = "", $peso = "", $cintura = "", $quadril = "", $envergadura = "", $flexibilidade = "", $resistencia_abdominal = "", $velocidade = "", $salto_em_discancia = "", $triceps = "", $subscapular = "", $resistencia_cardiovascular = "", $arremesso_de_medicine_ball = "", $agilidade = "") {
        $this->nome = $nome;
        $this->grupo = $grupo;
        $this->sexo = $sexo;
        $this->altura = (float) $altura;
        $this->peso = (float) $peso;
        $this->idade = $idade;
        $this->cintura = (float) $cintura;
        $this->quadril =(float) $quadril;
        $this->envergadura = (float) $envergadura;
        $this->flexibilidade = (float) $flexibilidade;
        $this->resistencia_abdominal = (float) $resistencia_abdominal;
        $this->velocidade = (float) $velocidade;
        $this->salto_em_discancia = (float) $salto_em_discancia;
        $this->triceps = (float) $triceps;
        $this->subscapular = (float) $subscapular;
        $this->resistencia_cardiovascular = (float) $resistencia_cardiovascular;
        $this->arremesso_de_medicine_ball = (float) $arremesso_de_medicine_ball;
        $this->agilidade = (float) $agilidade;
        $this->idAluno = $idAluno;
    }
            
    function getNome() {
        return $this->nome;
    }
    
    function getIdAluno() {
        return $this->idAluno;
    }
    function setIdAluno($idAluno) {
        $this->idAluno = $idAluno;
    }

    
    function getGrupo() {
        return $this->grupo;
    }

    function getSexo() {
        return $this->sexo;
    }

    function setSexo($sexo) {
        $this->sexo = $sexo;
    }

    function getAltura() {
        return $this->altura;
    }

    function getPeso() {
        return $this->peso;
    }

    function getIdade() {
        return $this->idade;
    }

    function getCintura() {
        return $this->cintura;
    }

    function getQuadril() {
        return $this->quadril;
    }

    function getEnvergadura() {
        return $this->envergadura;
    }

    function getFlexibilidade() {
        return $this->flexibilidade;
    }

    function getResistencia_abdominal() {
        return $this->resistencia_abdominal;
    }

    function getVelocidade() {
        return $this->velocidade;
    }

    function getSalto_em_discancia() {
        return $this->salto_em_discancia;
    }

    function getTriceps() {
        return $this->triceps;
    }

    function getSubscapular() {
        return $this->subscapular;
    }

    function getResistencia_cardiovascular() {
        return $this->resistencia_cardiovascular;
    }

    function getArremesso_de_medicine_ball() {
        return $this->arremesso_de_medicine_ball;
    }

    function getAgilidade() {
        return $this->agilidade;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }


    function setGrupo($grupo) {
        $this->grupo = $grupo;
    }

    function setAltura($altura) {
        $this->altura = $altura;
    }

    function setPeso($peso) {
        $this->peso = $peso;
    }

    function setIdade($idade) {
        $this->idade = $idade;
    }

    function setCintura($cintura) {
        $this->cintura = $cintura;
    }

    function setQuadril($quadril) {
        $this->quadril = $quadril;
    }

    function setEnvergadura($envergadura) {
        $this->envergadura = $envergadura;
    }

    function setFlexibilidade($flexibilidade) {
        $this->flexibilidade = $flexibilidade;
    }

    function setResistencia_abdominal($resistencia_abdominal) {
        $this->resistencia_abdominal = $resistencia_abdominal;
    }

    function setVelocidade($velocidade) {
        $this->velocidade = $velocidade;
    }

    function setSalto_em_discancia($salto_em_discancia) {
        $this->salto_em_discancia = $salto_em_discancia;
    }

    function setTriceps($triceps) {
        $this->triceps = $triceps;
    }

    function setSubscapular($subscapular) {
        $this->subscapular = $subscapular;
    }

    function setResistencia_cardiovascular($resistencia_cardiovascular) {
        $this->resistencia_cardiovascular = $resistencia_cardiovascular;
    }

    function setArremesso_de_medicine_ball($arremesso_de_medicine_ball) {
        $this->arremesso_de_medicine_ball = $arremesso_de_medicine_ball;
    }

    function setAgilidade($agilidade) {
        $this->agilidade = $agilidade;
    }



}
