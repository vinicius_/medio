<?php
/**
 * Objeto que abstrai o professor para esse problema
 */
class Professor {
    public $nome;
    private $cpf;
    private $telefone;
    public $email;
    private $senha;
    
    function __construct($nome, $cpf, $telefone, $email, $senha) {
        $this->nome = $nome;
        $this->cpf = $cpf;
        $this->telefone = $telefone;
        $this->email = $email;
        $this->senha = $senha;
    }
    
    /**
     * metodo necessário para a realização de serialização que será necessária para o armazenamento do objeto na sessão
     * @return array ('nome', 'cpf', 'telefone', 'email');
     */
    public function __sleep()
    {
        return array('nome', 'cpf', 'telefone', 'email');
    }
    
    
    function getNome() {
        return $this->nome;
    }

    function getCpf() {
        return $this->cpf;
    }

    function getTelefone() {
        return $this->telefone;
    }

    function getEmail() {
        return $this->email;
    }

    function getSenha() {
        return $this->senha;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    function setTelefone($telefone) {
        $this->telefone = $telefone;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setSenha($senha) {
        $this->senha = $senha;
    }

}
