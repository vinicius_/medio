<?php
require_once '../model/Aluno.php';
require '../dao/ClassificacaoDao.php';
/**
 * A classe Classificação é responsável por definir o resultado do aluno com base em seus valores, 
 * onde serão atribuidos [Excelente|Muito Bom| Bom | Razoavel| fraco] de acordo com as suas pontuações
 */
class Classificacao {
  
    private $aluno;
    
    private $arremesso_ball; //membros superiores
    private $salto; //membros inferiores
    private $corrida; //teste de velocidade
    private $resistencia; // resistencia aerobica (cardiovascular)
    private $agilidade; // 
    private $flexibilidade;
    private $resistencia_ab;
    
    public function __construct($aluno) { // construtor que executará a classificação numerica do aluno
        $this->aluno =  $aluno;
        $this->medicine_ball();
        $this->salto();
        $this->corrida(); 
        $this->resistencia_anaerobica(); //anaerobica e cardiovascular
        $this->agilidade();
        $this->flexibilidade();
        $this->resistencia_ab();
    }
    
    #medicine ball
    /**
     * Define a classificação numerica do aluno em medicine ball
     */
    private function medicine_ball(){
        $c = new ClassificacaoDao();//objeto do tipo classificaçãoDAO
        $res = null;
        if($this->aluno->getSexo() == 'm'){//verifica se é homem ou mulher
            
            $res = $c->m_medicine_ball($this->aluno->getIdade()); //pega os dados da especifica tabela baseado na idade do homem
            
        }else{
            
            $res = $c->f_medicine_ball($this->aluno->getIdade()); //pega os dados da especifica tabela baseado na idade da mulher
        }
        
        $this->arremesso_ball = $this->adequar($res, $this->aluno->getArremesso_de_medicine_ball()); 
    }
    
    #salto (em distancia)
    /**
     * Define a classificação numerica do aluno em salto
     */
    private function salto(){
        $c = new ClassificacaoDao();
        $res = null;
        
        if($this->aluno->getSexo() == 'm'){
            
            $res = $c->m_salto($this->aluno->getIdade());
        }else{
            
            $res = $c->f_salto($this->aluno->getIdade());
        }
        $this->salto = $this->adequar($res, $this->aluno->getSalto_em_discancia());
    }
    #corrida =  velocidade
    /**
     * Define a classificação numerica do aluno em corrida
     */
    private function corrida(){
        $c = new ClassificacaoDao();
        $res = null;
        $idade = $this->aluno->getIdade();
        $tempo = $this->aluno->getVelocidade();
        
        if($this->aluno->getSexo() == 'm'){
            
            $res = $c->m_velocidade($idade);
        }else{
            
            $res = $c->f_velocidade($idade);
        }
        $this->corrida = $this->readequar($res, $tempo);
    }
    
    
    #resistencia aerobica cardiovascular 
    /**
     * Define a classificação numerica do aluno em resistencia cardiovascular
     */
    private function resistencia_anaerobica(){
        $c = new ClassificacaoDao();
        $res = null;
        $idade = $this->aluno->getIdade();
        
        if($this->aluno->getSexo() == 'm'){
            
            $res = $c->m_resistencia($idade);
        }else{
            
            $res = $c->f_resistencia($idade);
        }
        $this->resistencia = $this->adequar($res, $this->aluno->getResistencia_cardiovascular());
        
    }

    #agilidade 
    /**
     * Define a classificação numerica do aluno em agilidade
     */
    public function agilidade(){
        $c = new ClassificacaoDao();
        $res = null;
        $idade = $this->aluno->getIdade();
        $tempo = $this->aluno->getAgilidade();
        
        if($this->aluno->getSexo() == 'm'){
            
            $res = $c->m_agilidade($idade);
        }else{
            
            $res = $c->f_agilidade($idade);
        }
        $this->agilidade = $this->readequar($res, $tempo);
        
    }
    /**
     * Define a classificação numerica do aluno em flexibilidade
     */
    public function flexibilidade(){
        $c = new ClassificacaoDao();
        $res = null;
        $idade = $this->aluno->getIdade();
        $valor = $this->aluno->getFlexibilidade();
        
        if($this->aluno->getSexo() == 'm'){
            
            $res = $c->m_flexibilidade($idade);
        }else{
            $res = $c->f_flexibilidade($idade);
        }
        
        $this->flexibilidade = $this->binar($res, $valor);
        
    }
    /**
     * Define a classificação numerica do aluno em resistencia abdominal 
     */
    public function resistencia_ab(){
        $c = new ClassificacaoDao();
        $res = null;
        $idade = $this->aluno->getIdade();
        $valor = $this->aluno->getResistencia_abdominal();
        
        if($this->aluno->getSexo() == 'm'){
            
            $res = $c->m_resistencia_ab($idade);
        }else{
            $res = $c->f_resistencia_ab($idade);
        }
        $this->resistencia_ab = $this->binar($res, $valor);
    }
    /**
     * Metodo feito apartir dos dados de flexibilidade e resistencia abdominal informados no manual proesp.
     * Ele classifica de forma 'binar'ia de acordo com a pontuação do aluno
     * @param array $ress
     * @param int $valor
     * @return string Excelente|Muito Bom| Bom | Razoavel| fraco
     */
    private function binar($ress, $valor){
        $res = $ress[0]; //o index 0 é referente a tupla dos dados retornados
        
        if($valor < $res['critico']){
            return "Bom";
        }
        return "Excelente";      
    }

    /**
     * pega um valor númérico e transforma em bom|razoabel|Excelente
     * @param array $ress o array com os parâmetros referentes a essa modalidade
     * @param int $valor o valor é a pontuação do aluno com relação a essa modalidade
     * @return string Excelente|Muito Bom| Bom | Razoavel| fraco
     */
    private function adequar($ress, $valor){
        $res = $ress[0];//o index 0 é referente a tupla dos dados retornados
        if($valor >= $res['exce_min'] ){
            return "Excelente";
        }else if($valor < $res['exce_min'] and $valor >= $res['mbom_min']){
            return "Muito Bom";
        }else if($valor < $res['mbom_min'] and $valor >= $res['bom_min']){
            return "Bom";
        }else if($valor < $res['bom_min'] and $valor >= $res['razoavel_min']){
            return "Razoavel";
        }else if($valor < $res['razoavel_min']){
            return "Fraco";
        }
        
    }
    /**
     * pega um valor númérico e transforma em bom|razoabel|Excelente, semelhante ao adequar porém é reestruturado para modalidades como agilidade e corrida
     * @param array $ress o array com os parâmetros referentes a essa modalidade
     * @param int $tempo o valor é a pontuação do aluno com relação a essa modalidade
     * @return string Excelente|Muito Bom| Bom | Razoavel| fraco
     */
    private function readequar($ress, $tempo){
        $res = $ress[0];
        if($tempo <= 0){
            return "Fraco";
        }
        if($tempo <= $res['exce_max']){
              return "Excelente";
        }else if($tempo > $res['exce_max'] and $res['mbom_max'] >= $tempo){
            
            return "Muito Bom";
        }else if($tempo > $res['mbom_max'] and $res['bom_max'] >= $tempo){
            
            return "Bom";
        }else if ($tempo > $res['bom_max'] and $res['razoavel_max'] >= $tempo){
            
            return "Razoavel";
        }else if($tempo > $res['razoavel_max'] ){
            
            return "Fraco";
        }else {
            return "Indefinido (m/Cl.)";
        }
    }
    
    
    function getArremesso_ball() {
        return $this->arremesso_ball;
    }

    function getSalto() {
        return $this->salto;
    }

    function getCorrida() {
        return $this->corrida;
    }

    function getResistencia_anaerobica() {
        return $this->resistencia;
    }

    function setArremesso_ball($arremesso_ball) {
        $this->arremesso_ball = $arremesso_ball;
    }

    function setSalto($salto) {
        $this->salto = $salto;
    }

    function setCorrida($corrida) {
        $this->corrida = $corrida;
    }

    function setResistencia($resistencia) {
        $this->resistencia = $resistencia;
    }
    
    function getAgilidade() {
        return $this->agilidade;
    }

    function setAgilidade($agilidade) {
        $this->agilidade = $agilidade;
    }

    function getFlexibilidade() {
        return $this->flexibilidade;
    }

    function getResistencia_ab() {
        return $this->resistencia_ab;
    }

    function setFlexibilidade($flexibilidade) {
        $this->flexibilidade = $flexibilidade;
    }

    function setResistencia_ab($resistencia_ab) {
        $this->resistencia_ab = $resistencia_ab;
    }




}
