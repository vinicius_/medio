<?php
require_once '../include/Config.php';
require_once '../dao/Conexao.php';
require_once '../model/Professor.php';
class ProfessorDao extends Conexao{
    private $query;//query que será executada no banco
    private $resultado;
    /**
     *
     * @var PDO 
     */
    private $conexao; //conexao PDO
    /**
     * Busca um professor especifico no banco com base no cpf e senha
     * @param String $cpf
     * @param String $senha
     */
    public function buscarProfessor($cpf, $senha){
        $this->query = "select * from professor where cpf = ? and senha = ?";
        
        $this->conexao = parent::getConexao();
        $this->query = $this->conexao->prepare($this->query);
        
        //combinando cada dado em suas respectivas posições na consulta
        $this->query->bindParam(1, $cpf);
        $this->query->bindParam(2, $senha);
        $this->executar();//executando a query
    }

    /**
     * Cadastra o professor no banco de dados
     * @param Professor $p
     */
    public function cadastrarProfessor(Professor $p){
        $this->query = "INSERT INTO professor (cpf, nome, telefone, email, senha) values (?,?,?,?,?)";
        
        $this->conexao = parent::getConexao();
        $this->query = $this->conexao->prepare($this->query);
        //armazenando os dados em variáveis locais
        $cpf = $p->getCpf();
        $nome = $p->getNome();
        $telefone = $p->getTelefone();
        $email = $p->getEmail();
        $senha = $p->getSenha();
        
        //combinando cada dado em suas respectivas posições na consulta
        $this->query->bindParam(1, $cpf);
        $this->query->bindParam(2, $nome);
        $this->query->bindParam(3, $telefone);
        $this->query->bindParam(4, $email);
        $this->query->bindParam(5, $senha);
        
        $this->executar();
    }
    
    private function executar(){
        
        try {
            $this->query->execute(); //executando query
            
        } catch (PDOException $exc) {
            //echo $exc->getTraceAsString();
            echo $exc->getMessage();
            echo '<h1>CPF Já cadastrado (dao/professorDAO)</h1>';
        }
    }
    
    public function getResultado(){
        //nao suportado ja que nao tem id
        $this->resultado = $this->query->fetchAll(); //obtendo resultados da execução
        return $this->resultado;
    }
    
}
