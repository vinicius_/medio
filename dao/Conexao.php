<?php
class Conexao {
    private static $host = HOST; // local onde está o banco de dados
    private static $usr = USER; // usuário que criou o banco de dados que será utilizado
    private static $pwd = PWD; // senha do banco de dados
    private static $db = DATABASE; // nome do banco de dados
    
    /**
     *
     * @var PDO 
     */
    private static $conexao = null; // variável que armazenará a conexão com o banco
    
    private static function conectar(){ // metodo para realizar a conexão com o banco de dados
        try {
            if (self::$conexao == null) { //verificando se a conexão está vazia
                    

                $dns = 'mysql:host='.self::$host.';dbname='.self::$db; // organizando o dns do PDO



                self::$conexao = new PDO($dns, self::$usr, self::$pwd); // criando uma conexão apartir dos dados informados
                }
        } catch (PDOException $exc) {
                echo $exc->getTraceAsString();
        }
        self::$conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return self::$conexao;
         
    }
    
    static function getConexao(){
        return self::conectar();
    }
    
}
