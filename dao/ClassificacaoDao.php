<?php

class ClassificacaoDao extends Conexao{
    private $query;//query que será executada no banco
    private $resultado; // resutado da query
    /**
     *
     * @var PDO 
     */
    private $conexao; //conexao do tipo PDO
    
    /**
     * Método para consulta dos parametros para o calculo de medicine ball masculina
     * @param int $idade
     * @return array
     */
    public function m_medicine_ball($idade){
        //ifs para a idades menores que 6 ou maiores que 17
        if($idade < 6){
           $idade = 6;
        }
        if($idade > 17){
           $idade = 17;
        }
        
        $this->query = "SELECT * FROM m_ms_mb where idade = ?";
        
        return $this->resto($idade); 
    }
    /**
     * Método para consulta dos parametros para o calculo de medicine ball feminina
     * @param int $idade
     * @return array
     */
    public function f_medicine_ball($idade){
        if($idade < 6){
           $idade = 6;
        }
        if($idade > 17){
           $idade = 17;
        }
        
        $this->query = "SELECT * FROM f_ms_mb where idade = ?";
        
        
        return $this->resto($idade);
    }
    /**
     * Método para consulta dos parametros para o calculo de salto masculina
     * @param int $idade
     * @return array
     */
    public function m_salto($idade){
        if($idade < 6){
           $idade = 6;
        }
        if($idade > 17){
           $idade = 17;
        }
        $this->query = "SELECT * FROM m_mi_sd where idade = ?";
        
        return $this->resto($idade);
    }
    /**
     * Método para consulta dos parametros para o calculo de salto feminino
     * @param int $idade
     * @return array
     */
    public function f_salto($idade){
        if($idade < 6){
           $idade = 6;
        }
        if($idade > 17){
           $idade = 17;
        }
        $this->query = "SELECT * FROM f_mi_sd where idade = ?";
        
        
        return $this->resto($idade);
    }

    /**
     * Método para consulta dos parametros para o calculo de velocidade masculina
     * @param int $idade
     * @return array
     */
    public function m_velocidade($idade){
        if($idade < 6){
           $idade = 6;
        }
        if($idade > 17){
           $idade = 17;
        }
        $this->query = "SELECT * FROM m_t_velocidade where idade = ?";
        
        
        return $this->resto($idade);
    }
    
    
    /**
     * Método para consulta dos parametros para o calculo de velocidade feminina
     * @param int $idade
     * @return array
     */
    public function f_velocidade($idade){
        if($idade < 6){
           $idade = 6;
        }
        if($idade > 17){
           $idade = 17;
        }
        $this->query = "SELECT * FROM f_t_velocidade where idade = ?";
        
        
        return $this->resto($idade);
    }
    
    /**
     * Método para consulta dos parametros para o calculo de resistencia masculina
     * @param int $idade
     * @return array
     */
    public function m_resistencia($idade){
        if($idade < 6){
           $idade = 6;
        }
        if($idade > 17){
           $idade = 17;
        }
        $this->query = "SELECT * FROM m_t_resistencia where idade = ?";
        
        
        return $this->resto($idade);
    }
    /**
     * Método para consulta dos parametros para o calculo de resistencia feminina
     * @param int $idade
     * @return array
     */
    public function f_resistencia($idade){
        if($idade < 6){
           $idade = 6;
        }
        if($idade > 17){
           $idade = 17;
        }
        $this->query = "SELECT * FROM f_t_resistencia where idade = ?";
        
        
        return $this->resto($idade);
    }

    /**
     * Método para consulta dos parametros para o calculo de agilidade masculina
     * @param int $idade
     * @return array
     */
    public function m_agilidade($idade){
        if($idade < 6){
           $idade = 6;
        }
        if($idade > 17){
           $idade = 17;
        }
        $this->query = "SELECT * FROM m_t_agilidade where idade = ?";
        
        
        return $this->resto($idade);
    }
    
    /**
     * Método para consulta dos parametros para o calculo de agilidade feminina
     * @param int $idade
     * @return array
     */
    public function f_agilidade($idade){
        if($idade < 6){
           $idade = 6;
        }
        if($idade > 17){
           $idade = 17;
        }
        $this->query = "SELECT * FROM f_t_agilidade where idade = ?";
         
        
        return $this->resto($idade);
    }
    
    /**
     * Método para consulta dos parametros para o calculo de flexibilidade masculina
     * @param int $idade
     * @return array
     */
    public function m_flexibilidade($idade){
        if($idade < 6){
           $idade = 6;
        }
        if($idade > 17){
           $idade = 17;
        }
        $this->query = "SELECT * FROM m_t_flexibilidade where idade = ?";
        
        
        return $this->resto($idade);
    }
    /**
     * Método para consulta dos parametros para o calculo de agilidade feminina
     * @param int $idade
     * @return array
     */
    public function f_flexibilidade($idade){
        if($idade < 6){
           $idade = 6;
        }
        if($idade > 17){
           $idade = 17;
        }
        $this->query = "SELECT * FROM f_t_flexibilidade where idade = ?";
        
        
        return $this->resto($idade);
    }
    /**
     * Método para consulta dos parametros para o calculo de resistenci abdominal masculina
     * @param int $idade
     * @return array
     */
    public function m_resistencia_ab($idade){
        if($idade < 6){
           $idade = 6;
        }
        if($idade > 17){
           $idade = 17;
        }
        $this->query = "SELECT * FROM m_t_resistencia_ab where idade = ?";
        
        
        return $this->resto($idade);
    }
    
    /**
     * Método para consulta dos parametros para o calculo de resistenci abdominal feminina
     * @param int $idade
     * @return array
     */
    public function f_resistencia_ab($idade){
        if($idade < 6){
           $idade = 6;
        }
        if($idade > 17){
           $idade = 17;
        }
        $this->query = "SELECT * FROM f_t_resistencia_ab where idade = ?";
        
        return $this->resto($idade);
    }
    /**
     * O método é responsável por fazer o resto das configurações para efetuar a consulta
     * @param int $idade
     * @return type
     */
    public function resto($idade){
        if ($this->conexao == null) {
            $this->conexao = parent::getConexao();
        }
        $this->query = $this->conexao->prepare($this->query);
        $this->query->bindParam(1, $idade);
        $this->executar();
        return $this->getResultado();
        
    }

    private function executar() {

        try {
            $this->query->execute();//execução da query
        } catch (PDOException $exc) {
            //echo $exc->getTraceAsString();
            echo $exc->getMessage();
            echo '<h1>PRoblemA</h1>';
            die();
        }
    }

    public function getResultado() {
        $this->resultado = $this->query->fetchAll(); //resultado da query
        return $this->resultado;
    }

}
