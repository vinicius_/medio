<?php
require_once '../include/Config.php';
require_once '../dao/Conexao.php';
require_once '../model/Aluno.php';

/*
 * Classe que é responsável por gerenciar dos dados do aluno no BD
 */
class AlunoDao extends Conexao{
    private $query; // variavel quer armazenará a query que será executada no banco
    
    private $resultado; // variável que armazenará o resultado das consultas
    /**
     *
     * @var PDO 
     */
    private $conexao; // conexao com o banco
    
    public function cadastrarAluno(Aluno $a, $prof_cpf){
        $this->query = "INSERT INTO aluno(nome, dataNasc,grupo,sexo,cpf_prof) values (?,?,?,?,?)"; //consulta que será executada no banco
        
        $this->conexao = parent::getConexao(); //obtendo a conxeão com o banco
        $this->query = $this->conexao->prepare($this->query); // preparando a consulta na conexao
        
        //armazenando os dados em variáveis locais
        //$cpf_aluno = $a->getCpf();
        $nome = $a->getNome();
        $idade = $a->getIdade();
        $grupo = $a->getGrupo();
        $sexo = $a->getSexo();
        
        //combinando cada dado em suas respectivas posições na consulta
        //$this->query->bindParam(1, $cpf_aluno);
        $this->query->bindParam(1, $nome);
        $this->query->bindParam(2, $idade);
        $this->query->bindParam(3, $grupo);
        $this->query->bindParam(4, $sexo);
        $this->query->bindParam(5, $prof_cpf);
        
       
        $this->executar();  //executando a query
       
        $this->cadstrarCadastro($a); //cadastrando o restante dos dados
    }
    private function cadstrarCadastro(Aluno $a){
        $this->query = "INSERT INTO cadastro_aluno(altura,peso,cintura,quadril,envergadura,flexibilidade,resistencia_abdominal,velocidade,salto_distancia,triceps,subscapular,resistencia_cardiovascular,arremesso_medicine_ball,agilidade,fk_aluno) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; //query que será executada no banco
        $this->query = $this->conexao->prepare($this->query); //preparando a query
        
        //armazenando os dados em variáveis locais

        
        $altura = $a->getAltura();
        $peso = $a->getPeso();
        $cintura = $a->getCintura();
        $quadril = $a->getQuadril();
        $envergadura = $a->getEnvergadura();
        $flexibi = $a->getFlexibilidade();
        $resisAb = $a->getResistencia_abdominal();
        $velocidad = $a->getVelocidade();
        $salto = $a->getSalto_em_discancia();
        $triceps = $a->getTriceps();
        $subsca = $a->getSubscapular();
        $resisCar = $a->getResistencia_cardiovascular();
        $arremesso = $a->getArremesso_de_medicine_ball();
        $agilida = $a->getAgilidade();
       
        
        $fk_aluno = (int) $this->conexao->lastInsertId();
        
        $numb = 1; // variavel que será incrementada abaixo
        
        //combinando cada dado em suas respectivas posições na consulta
        $this->query->bindParam($numb++, $altura);
        $this->query->bindParam($numb++, $peso);
        $this->query->bindParam($numb++, $cintura);
        $this->query->bindParam($numb++, $quadril);
        $this->query->bindParam($numb++, $envergadura);
        $this->query->bindParam($numb++, $flexibi);
        $this->query->bindParam($numb++, $resisAb);
        $this->query->bindParam($numb++, $velocidad);
        $this->query->bindParam($numb++, $salto);
        $this->query->bindParam($numb++, $triceps);
        $this->query->bindParam($numb++, $subsca);
        $this->query->bindParam($numb++, $resisCar);
        $this->query->bindParam($numb++, $arremesso);
        $this->query->bindParam($numb++, $agilida);
        $this->query->bindParam($numb++, $fk_aluno);
        
        $this->executar();
    }


    
    private function executar() {
 
        try {
            $this->query->execute(); //executando a query
        } catch (PDOException $exc) {
            //echo $exc->getTraceAsString();
            echo $exc->getMessage();
            echo '<h1>PRoblemA</h1>';
            die();
        }
    }
    
    public function getResultado(){
        $this->resultado = $this->query->fetchAll(); //capturando o resutado da query 
        return $this->resultado; // retornando esses dados
    }
    
    /**
     * Método para buscar todos os alunos pertencentes a um professor
     * @param String $cpf_prof
     * @return array
     */
    public function allAlunos($cpf_prof){
        $this->query = "SELECT * FROM aluno a join grupo g on a.grupo = g.idGrupo where cpf_prof = ? "; //query que será execultada no banco
        if ($this->conexao == null) { //verificando se a conexão está vazia
            $this->conexao = parent::getConexao();
        }
        $this->query = $this->conexao->prepare($this->query); //preparanco a query na conexão
        $this->query->bindParam(1, $cpf_prof); //combinando o dado em sua posição na consulta
        $this->executar();// executando a consulta
        return $this->getResultado(); //retornando o resultado da consulta
    }

    /**
     * Método para buscar todos os alunos de um determinado grupo e pertencentes a um professor
     * @param String $cpf_prof
     * @return array
     */
    public function filtroPorGrupo($cpf_prof, $id_grupo){
        $this->query = "SELECT * FROM aluno a join grupo g on a.grupo = g.idGrupo where cpf_prof = ? and idGrupo = ? "; //query que será execultada no banco
        if ($this->conexao == null) { //verificando se a conexão está vazia
            $this->conexao = parent::getConexao();
        }
        $this->query = $this->conexao->prepare($this->query); //preparanco a query na conexão
        $this->query->bindParam(1, $cpf_prof); //combinando o dado em sua posição na consulta
        $this->query->bindParam(2, $id_grupo);
        $this->executar();// executando a consulta
        return $this->getResultado(); //retornando o resultado da consulta
    }

    /**
     * Método para buscar o aluno e todos os seus dados, mesmo funcionamento que o metodo allAlunos
     * @param String $cpf_prof
     * @param int $idAluno
     * @return Array
     */
    public function getAlunoFull($cpf_prof, $idAluno){
        $this->query = "select * from alunofull where idAluno = ? and cpf_prof = ?";
        if ($this->conexao == null) {
            $this->conexao = parent::getConexao();
        }
        $this->query = $this->conexao->prepare($this->query);
        $this->query->bindParam(1, $idAluno);
        $this->query->bindParam(2, $cpf_prof);
        $this->executar();
        return $this->getResultado();
    }

    
    /**
     * Método para gerar um objeto do tipo aluno a partir de um array de aluno
     * @param array $array
     * @return \Aluno
     */
    public function objAluno(array $array){
        $rey = $array[0];
        $a = new Aluno($rey['nome'] , $this->idade($rey['dataNasc']),$rey['grupo'], $rey['sexo']);
        $a->setIdAluno($rey['idAluno']);
        //$a->setIdade($rey['idade']);
        $a->setAltura($rey['altura']);
        $a->setPeso($rey['peso']);
        $a->setCintura($rey['cintura']);
        $a->setQuadril($rey['quadril']);
        $a->setEnvergadura($rey['envergadura']);
        $a->setFlexibilidade($rey['flexibilidade']);
        $a->setResistencia_abdominal($rey['resistencia_abdominal']);
        $a->setVelocidade($rey['velocidade']);
        $a->setSalto_em_discancia($rey['salto_distancia']);
        $a->setTriceps($rey['triceps']);
        $a->setSubscapular($rey['subscapular']);
        $a->setResistencia_cardiovascular($rey['resistencia_cardiovascular']);
        $a->setArremesso_de_medicine_ball($rey['arremesso_medicine_ball']);
        $a->setAgilidade($rey['agilidade']);
        $a->setAgilidade($rey['agilidade']);
        return $a;
    }
    
    public function idade($nasc){
        $birthDate = explode("/", $nasc);
        //get age from date or birthdate
        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y") - $birthDate[2]) - 1) : (date("Y") - $birthDate[2]));
        return $age;
    }

}
