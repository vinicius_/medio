
create database medio character set UTF8mb4 collate utf8mb4_general_ci;

use medio;

create table professor(
    cpf varchar(20) not null unique primary key,
    nome varchar(64) not null,
    telefone varchar(16) not null,
    email varchar(64) not null,
    senha varchar(32) not null
);


create table aluno(
	idAluno int auto_increment primary key,
    nome varchar(64) not null,
    dataNasc date not null,
    grupo int not null,
    sexo char not null,
    cpf_prof varchar(20) not null,
    foreign key (cpf_prof) references professor(cpf),
    foreign key (grupo) references grupo(idGrupo)
);

create table cadastro_aluno(
	id_aluno int not null primary key auto_increment,
	altura float not null default 0.0,
	peso float not null default 0.0,
	cintura float not null default 0.0,
	quadril float not null default 0.0,
	envergadura float not null default 0.0,
	flexibilidade float not null default 0.0,
	resistencia_abdominal float not null default 0.0,
	velocidade float not null,
	salto_distancia float not null,
	triceps float not null,
	subscapular float not null,
	resistencia_cardiovascular float not null,
	arremesso_medicine_ball float not null,
	agilidade float not null,
	fk_Aluno int not null,
	foreign key (fk_Aluno) references aluno(idAluno)
);

create table grupo(
idGrupo int auto_increment primary key,
nomeGrupo varchar(25)
);

insert into grupo(nomeGrupo) values 
('1º Ano Biotecnologia'),
('2º Ano Biotecnologia'),
('3º Ano Biotecnologia'),
('1º Ano Informática'),
('2º Ano Informática'),
('3º Ano Informática');

create or replace view alunofull as
 select  a.nome, g.nomeGrupo as 'grupo',DATE_FORMAT(a.dataNasc,'%d/%c/%Y') as 'dataNasc', a.sexo, a.idAluno, a.cpf_prof,c.* from cadastro_aluno c join aluno a on a.idAluno = c.fk_aluno join grupo g on a.grupo = g.idGrupo;

#


create table m_ms_mb(
	idade int not null,
    razoavel_min int not null,
    bom_min int not null,
    mbom_min int not null,
    exce_min int not null
);
						#		razo	bom		mb	exce
insert into m_ms_mb values
#		razo	bom		mb	exce
(6,    145,   160,   183,  240),
(7,    164,   180,   202,  250),
(8,    180,   200,   225,  270),
(9,    200,   220,   250,  300),
(10,    213,   240,   270,  330),
(11,    238,   261,   294,  362),
(12,    264,   297,   330,  423),
(13,    300,   340,   390,  500),
(14,    350,   400,   450,  562),
(15,    400,   440,   500,  609),
(16,    453,   500,   553,  700),
(17,    480,   520,   590,  690);

create table f_ms_mb(
	idade int not null,
    razoavel_min int not null,
    bom_min int not null,
    mbom_min int not null,
    exce_min int not null
);

insert into f_ms_mb values
#		razo	bom		mb	exce
(6,		140,	150,	164,	208),
(7,		153,	162,	180,	217),
(8,		167,	185,	200,	247),
(9,		185,	201,	226,	280),
(10,	200,	220,	245,	302),
(11,	220,	247,	275,	330),
(12,	241,	270,	300,	370),
(13,	265,	295,	323,	400),
(14,	280,	310,	344,	418),
(15,	300,	330,	360,	430),
(16,	320,	340,	370,	450),
(17,	310,	340,	375,	441);

create table m_mi_sd(
	idade int not null,
    razoavel_min int not null,
    bom_min int not null,
    mbom_min int not null,
    exce_min int not null
);

insert into m_mi_sd values
#		razo	bom		mb	exce
(6, 	105, 	115,	128,	151),
(7,		111,	122,	134,	160),
(8,		118,	128,	140,	166),
(9,		129,	140,	152,	179),
(10,	135,	147,	158,	188),
(11,	140,	152,	165,	192),
(12,	149,	160,	174,	204),
(13,	159,	170,	185,	217),
(14,	170,	184,	200,	231),
(15,	180,	194,	210,	243),
(16,	186,	200,	215,	249),
(17,	186,	204,	220,	251);

create table f_mi_sd(
    idade int not null,
    razoavel_min int not null,
    bom_min int not null,
    mbom_min int not null,
    exce_min int not null
);

insert into f_mi_sd values
#		razo	bom		mb	exce
(6,		90,		101,	112,	144),
(7,		94,		106,	116,	147),
(8,		105,	113,	127,	153),
(9,		116,	127,	140,	166),
(10,	123,	134,	146,	174),
(11,	127,	138,	150,	180),
(12,	130,	141,	155,	185),
(13,	133,	145,	160,	190),
(14,	134,	147,	161,	199),
(15,	135,	148,	163,	199),
(16,	131,	143,	159,	192),
(17,	121,	135,	153,	190);


create table m_t_agilidade(
	idade int not null,
    exce_max float not null,
    mbom_max float not null,
    bom_max float not null,
    razoavel_max float not null    
);

insert into m_t_agilidade values
#		exce	mb		bom		razo
(6,		6.40,	7.30,	7.79,	8.19),
(7,		6.07,	7.00,	7.43,	7.76),
(8,		5.97,	6.78,	7.20,	7.59),
(9,		5.81,	6.50,	6.89,	7.19),
(10,	5.58,	6.25,	6.66,	7.00),
(11,	5.39,	6.10,	6.50,	6.87),
(12,	5.17,	6.00,	6.34,	6.70),
(13,	5.00,	5.86,	6.16,	6.53),
(14,	5.00,	5.69,	6.00,	6.37),
(15,	4.91,	5.59,	5.99,	6.26),
(16,	4.90,	5.42,	5.75,	6.10),
(17,	4.90,	5.43,	5.75,	6.03);


create table f_t_agilidade(
	idade int not null,
    exce_max float not null,
    mbom_max float not null,
    bom_max float not null,
    razoavel_max float not null    
);

insert into f_t_agilidade values
#		exce	mb		bom		razo
(6, 	6.58,	7.66,	8.26,	8.68),
(7,		6.56,	7.56,	8.00,	8.41),
(8,		6.40,	7.22,	7.59,	7.98),
(9,		6.03,	6.89,	7.25,	7.63),
(10,	5.88,	6.60,	7.00,	7.35),
(11,	5.72,	6.49,	6.90,	7.24),
(12,	5.63,	6.36,	6.80,	7.17),
(13,	5.57,	6.28,	6.70,	7.10),
(14,	5.49,	6.22,	6.68,	7.03),
(15,	5.33,	6.19,	6.66,	7.00),
(16,	5.41,	6.15,	6.55,	6.94),
(17,	5.54,	6.22,	6.58,	7.00);

create table m_t_velocidade(
	idade int not null,
    exce_max float not null,
    mbom_max float not null,
    bom_max float not null,
    razoavel_max float not null    
);

insert into m_t_velocidade values
#		exce	mb		bom		razo
(6,		3.72,	4.20,	4.53,	4.80),
(7,		3.65,	4.12,	4.42,	4.62),
(8,		3.50,	4,		4.21,	4.47),
(9,		3.15,	3.88,	4.09,	4.31),
(10,	3.07,	3.74,	3.98,	4.15),
(11,	3,		3.62,	3.86,	4.03),
(12,	3,		3.50,	3.74,	3.96),
(13,	3,		3.37,	3.60,	3.81),
(14,	2.90,	3.23,	3.46,	3.67),
(15,	2.87,	3.16,	3.38,	3.60),
(16,	2.78,	3.12,	3.31,	3.50),
(17,	2.72,	3.12,	3.30,	3.53);


create table f_t_velocidade(
	idade int not null,
    exce_max float not null,
    mbom_max float not null,
    bom_max float not null,
    razoavel_max float not null    
);

insert into f_t_velocidade values
#		exce	mb		bom		razo
(6,		4.01,	4.54,	4.83,	5.11),
(7,		3.90,	4.47,	4.77,	5.07),
(8,		3.87,	4.27,	4.77,	5.07),
(9,		3.55,	4,		4.28,	4.54),
(10,	3.43,	3.97,	4.16,	4.41),
(11,	3.29,	3.87,	4.09,	4.31),
(12,	3.07,	3.78,	4,		4.25),
(13,	3,		3.71,	3.98,	4.19),
(14,	3,		3.70,	3.97,	4.21),
(15,	3.05,	3.72,	4,		4.25),
(16,	3.24,	3.70,	4,		4.23),
(17,	3.16,	3.79,	4.07,	4.32);

create table m_t_resistencia(
	idade int not null,
    razoavel_min int not null,
    bom_min int not null,
    mbom_min int not null,
    exce_min int not null
);

insert into m_t_resistencia values
#		razo	bom		mb	exce
(6, 	690,	741,	781,	879),
(7,		735,	786,	825,	924),
(8,		773,	826,	879,	1010),
(9,		845,	900,	966,	1097),
(10,	880,	942,	1010,	1158),
(11,	915,	978,	1050,	1190),
(12,	965,	1030,	1100,	1255),
(13,	983,	1083,	1159,	1320),
(14,	1068,	1135,	1210,	1372),
(15,	1120,	1187,	1262,	1435),
(16,	1150,	1220,	1289,	1505),
(17,	1156,	1220,	1289,	1506);


create table f_t_resistencia(
	idade int not null,
    razoavel_min int not null,
    bom_min int not null,
    mbom_min int not null,
    exce_min int not null
);

insert into f_t_resistencia values
#		razo	bom		mb	exce
(6,		612,	641,	681,	832),
(7,		652,	683,	730,	852),
(8,		700,	735,	778,	875),
(9,		750,	790,	841,	966),
(10,	783,	832,	884,	1027),
(11,	822,	868,	920,	1043),
(12,	855,	901,	958,	1081),
(13,	887,	935,	997,	1129),
(14,	920,	967,	1024,	1164),
(15,	955,	1000,	1044,	1205),
(16,	970,	1010,	1055,	1156),
(17,	982,	1023,	1063,	1207);


create table m_t_flexibilidade(
	idade int not null,
    critico float not null
);

insert into m_t_flexibilidade values
(6, 29.3),
(7, 29.3),
(8, 29.3),
(9, 29.3),
(10, 29.4),
(11, 27.8),
(12, 24.7),
(13, 23.1),
(14, 22.9),
(15, 24.3),
(16, 25.7),
(17, 25.7);

create table f_t_flexibilidade(
	idade int not null,
    critico float not null
);

insert into f_t_flexibilidade values
(6,21.4),
(7,21.4),
(8,21.4),
(9,21.4),
(10,23.5),
(11,23.5),
(12,23.5),
(13,23.5),
(14,24.3),
(15,24.3),
(16,24.3),
(17,24.3);

create table m_t_resistencia_ab(
	idade int not null,
    critico float not null
);

insert into m_t_resistencia_ab values
(6,20),
(7,20),
(8,20),
(9,22),
(10,22),
(11,25),
(12,30),
(13,35),
(14,35),
(15,35),
(16,40),
(17,40);

create table f_t_resistencia_ab(
	idade int not null,
    critico float not null
);

insert into f_t_resistencia_ab values
(6,20),
(7,20),
(8,20),
(9,20),
(10,20),
(11,20),
(12,20),
(13,23),
(14,23),
(15,23),
(16,23),
(17,23);