<?php
session_start();//iniciando a sessão
if (isset($_SESSION['cpf'])) {//verificando se o professor está logado
    //echo '<i>Professor logado</i><br>';
} else {
    echo 'O professor nao foi logado';
    die();
}

//importação das classes necessárias
require '../model/Professor.php';
require '../scripts/scripts.php';
require '../dao/ProfessorDao.php';

$p = unserialize($_SESSION['professor']); //deserializando o objeto com os dados do professor
?>
<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Home - Professor</title>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <link rel="stylesheet" href="css/generalstyle.css">
        <link rel="stylesheet" href="css/professorstyle.css">

    </head>

    <body>

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" id="logo" href="../index.php">SCADAF</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="logout.php">Sair</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="wrapper" class="toggled ">
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <li class="sidebar-brand bg-secondary">
                        <a href="#" class="text-light">
                            Home
                        </a>
                    </li>
                    <li>
                        <a href="alunos.php">Alunos</a>
                    </li>
                    <li>
                        <a href="esportes.php">Esportes</a>
                    </li>
                    <li>
                        <a href="testes.php">Testes</a>
                    </li>
                </ul>
            </div>

            <div id="page-content-wrapper">
                <div class="container-fluid">

                    <div class="jumbotron">
                        <h1 class="display-4"><?php echo períodoDia() . ", " . $p->getNome() . "!" ?> </h1>
                        <p class="lead">Essa é a sua plataforma de administração dos seus alunos de educação física.</p>
                        <hr class="my-4">
                        <p>Use a barra de navegação lateral para ir para as áreas dos alunos, dos esportes e dos testes de aptidão. Para cadastrar um novo aluno, clique no botão abaixo.</p>
                        <a href="cadastrarAluno.php" class="btn btn-primary btn-lg btn-block text-white mt-5">Cadastro de Alunos</a>
                    </div>
                    


                </div>
            </div>

        </div>
    </body>

</html>
<?php

