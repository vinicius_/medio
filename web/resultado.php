<?php
//importação das classes necessárias
require_once '../dao/AlunoDao.php';
require_once '../model/Classificacao.php';
require_once '../model/Esporte.php';
require_once '../scripts/scripts.php';

session_start();//iniciando a sessão                                                //// a parte das caracteristicas nao foi finalizado
$cpf_prof = $_SESSION['cpf'];// aramzenando o cpf do professor em uma variavel local

$post = filter_input_array(INPUT_POST, FILTER_DEFAULT); //filtrando os dados do POST para uma variável local

if(!isset($cpf_prof)){//verificando se o professor está logado
    echo "<h3>Professor nao logado (web/resultado)</h3>";
    die();
}

$daoA = new AlunoDao();//criando um objeto do tipo aluno
$aluno = $daoA->objAluno($daoA->getAlunoFull($cpf_prof, $post['idAluno'])); //obj com todos os dados do alunno
$classi = new Classificacao($aluno); //classificando os dados do aluno
$esporte = new Esporte($classi, 1); //classificando o aluno nos esportes

/**
 * ifs para a definição de exibição
 */
if($post['esporte'] == 'futsal'){
    $aprov = $esporte->futebol;
}else 
if($post['esporte'] == 'atletismo'){
    $aprov = $esporte->atletismo;
}
else 
if($post['esporte'] == 'basquete'){
    $aprov = $esporte->basquete;
}
else 
if($post['esporte'] == 'handebol'){
    $aprov = $esporte->handbol;
}
else 
if($post['esporte'] == 'volei'){
    $aprov = $esporte->volei;
}
?>
<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Resultado</title>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <link rel="stylesheet" href="css/generalstyle.css">
        <link rel="stylesheet" href="css/testesstyle.css">

    </head>

    <body>

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" id="logo" href="../index.php">SCADAF</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#">Meu perfil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="logout.php">Sair</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        
        <?php
        
        if($aprov){
            
        
        ?>
        <div class="container mt-5">
            <div class="jumbotron alert-success">
                <div class="container text-center mb-5">
                    <h3><?=$aluno->getNome()?></h3>
                    <h3>x</h3>
                    <h3><?=$post['esporte']?></h3>
                </div>
                <hr class="my-4">
                <h1 class="text-center">O aluno é apto!</h1>
                <p class="lead">De acordo com a nossa análise sobre as informações cadastradas, o aluno possui boas condições físicas para a realização desse esporte! Mais detalhes abaixo.</p>
                <hr class="my-4">
                <label>Flexibilidade</label>
                <div class="progress mb-3">
                    <div class="progress-bar <?= barraBg($classi->getFlexibilidade()) ?>" role="progressbar" aria-valuenow="<?= nivel($classi->getFlexibilidade()) ?>"
                         aria-valuemin="0" aria-valuemax="100" style="width:<?= nivel($classi->getFlexibilidade()) ?>%">
                        <?= $classi->getFlexibilidade() ?>
                    </div>
                </div>

                <label>Agilidade</label>
                <div class="progress mb-3">
                    <div class="progress-bar <?= barraBg($classi->getAgilidade()) ?>" role="progressbar" aria-valuenow="<?= nivel($classi->getAgilidade()) ?>"
                         aria-valuemin="0" aria-valuemax="100" style="width:<?= nivel($classi->getAgilidade()) ?>%">
                        <?= $classi->getAgilidade() ?>
                    </div>
                </div>
                
                <label>Força Explosiva (Membros Superiores)</label>
                <div class="progress mb-3">
                    <div class="progress-bar <?= barraBg($classi->getArremesso_ball()) ?>" role="progressbar" aria-valuenow="<?= nivel($classi->getArremesso_ball()) ?>"
                         aria-valuemin="0" aria-valuemax="100" style="width:<?= nivel($classi->getArremesso_ball()) ?>%">
                        <?= $classi->getArremesso_ball() ?>
                    </div>
                </div>
                
                <label>Velocidade</label>
                <div class="progress mb-3">
                    <div class="progress-bar <?= barraBg($classi->getCorrida()) ?>" role="progressbar" aria-valuenow="<?= nivel($classi->getCorrida()) ?>"
                         aria-valuemin="0" aria-valuemax="100" style="width:<?= nivel($classi->getCorrida()) ?>%">
                        <?= $classi->getCorrida() ?>
                    </div>
                </div>
                
                <label>Resistência Muscular Localizada</label>
                <div class="progress mb-3">
                    <div class="progress-bar <?= barraBg($classi->getResistencia_ab()) ?>" role="progressbar" aria-valuenow="<?= nivel($classi->getResistencia_ab()) ?>"
                         aria-valuemin="0" aria-valuemax="100" style="width:<?= nivel($classi->getResistencia_ab()) ?>%">
                        <?= $classi->getResistencia_ab() ?>
                    </div>
                </div>
                
                <label>Resistência Anaeróbica</label>
                <div class="progress mb-3">
                    <div class="progress-bar <?= barraBg($classi->getResistencia_anaerobica()) ?>" role="progressbar" aria-valuenow="<?= nivel($classi->getResistencia_anaerobica()) ?>"
                         aria-valuemin="0" aria-valuemax="100" style="width:<?= nivel($classi->getResistencia_anaerobica()) ?>%">
                        <?= $classi->getResistencia_anaerobica() ?>
                    </div>
                </div>
                
                <label>Força Explosiva (Membros Inferiores)</label>
                <div class="progress mb-3">
                    <div class="progress-bar <?= barraBg($classi->getSalto()) ?>" role="progressbar" aria-valuenow="<?= nivel($classi->getSalto()) ?>"
                         aria-valuemin="0" aria-valuemax="100" style="width:<?= nivel($classi->getSalto()) ?>%">
                        <?= $classi->getSalto() ?>
                    </div>
                </div>
            </div> 
        </div>
        <?php
        }else{
            
        
        ?>
        <div class="container mt-5">
            <div class="jumbotron alert-danger">
                <div class="container text-center mb-5">
                    <h3><?=$aluno->getNome()?></h3>
                    <h3>x</h3>
                    <h3><?=$post['esporte']?></h3>
                </div>
                <hr class="my-4">
                <h1 class="text-center">O aluno não é apto!</h1>
                <p class="lead">De acordo com a nossa análise sobre as informações cadastradas, o aluno não possui boas condições físicas para a realização desse esporte! Mais detalhes abaixo.</p>
                <hr class="my-4">
                <label>Flexibilidade</label>
                <div class="progress mb-3">
                    <div class="progress-bar <?= barraBg($classi->getFlexibilidade()) ?>" role="progressbar" aria-valuenow="<?= nivel($classi->getFlexibilidade()) ?>"
                         aria-valuemin="0" aria-valuemax="100" style="width:<?= nivel($classi->getFlexibilidade()) ?>%">
                        <?= $classi->getFlexibilidade() ?>
                    </div>
                </div>

                <label>Agilidade</label>
                <div class="progress mb-3">
                    <div class="progress-bar <?= barraBg($classi->getAgilidade()) ?>" role="progressbar" aria-valuenow="<?= nivel($classi->getAgilidade()) ?>"
                         aria-valuemin="0" aria-valuemax="100" style="width:<?= nivel($classi->getAgilidade()) ?>%">
                        <?= $classi->getAgilidade() ?>
                    </div>
                </div>
                
                <label>Força Explosiva (Membros Superiores)</label>
                <div class="progress mb-3">
                    <div class="progress-bar <?= barraBg($classi->getArremesso_ball()) ?>" role="progressbar" aria-valuenow="<?= nivel($classi->getArremesso_ball()) ?>"
                         aria-valuemin="0" aria-valuemax="100" style="width:<?= nivel($classi->getArremesso_ball()) ?>%">
                        <?= $classi->getArremesso_ball() ?>
                    </div>
                </div>
                
                <label>Velocidade</label>
                <div class="progress mb-3">
                    <div class="progress-bar <?= barraBg($classi->getCorrida()) ?>" role="progressbar" aria-valuenow="<?= nivel($classi->getCorrida()) ?>"
                         aria-valuemin="0" aria-valuemax="100" style="width:<?= nivel($classi->getCorrida()) ?>%">
                        <?= $classi->getCorrida() ?>
                    </div>
                </div>
                
                <label>Resistência Muscular Localizada</label>
                <div class="progress mb-3">
                    <div class="progress-bar <?= barraBg($classi->getResistencia_ab()) ?>" role="progressbar" aria-valuenow="<?= nivel($classi->getResistencia_ab()) ?>"
                         aria-valuemin="0" aria-valuemax="100" style="width:<?= nivel($classi->getResistencia_ab()) ?>%">
                        <?= $classi->getResistencia_ab() ?>
                    </div>
                </div>
                
                <label>Resistência Anaeróbica</label>
                <div class="progress mb-3">
                    <div class="progress-bar <?= barraBg($classi->getResistencia_anaerobica()) ?>" role="progressbar" aria-valuenow="<?= nivel($classi->getResistencia_anaerobica()) ?>"
                         aria-valuemin="0" aria-valuemax="100" style="width:<?= nivel($classi->getResistencia_anaerobica()) ?>%">
                        <?= $classi->getResistencia_anaerobica() ?>
                    </div>
                </div>
                
                <label>Força Explosiva (Membros Inferiores)</label>
                <div class="progress mb-3">
                    <div class="progress-bar <?= barraBg($classi->getSalto()) ?>" role="progressbar" aria-valuenow="<?= nivel($classi->getSalto()) ?>"
                         aria-valuemin="0" aria-valuemax="100" style="width:<?= nivel($classi->getSalto()) ?>%">
                        <?= $classi->getSalto() ?>
                    </div>
                </div>
            </div>  
        </div>
        <?php
        }
        ?>
    </div>
</body>
</html>
