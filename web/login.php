<?php
include '../include/Config.php'; //importando as configurações
session_start(); // iniciando a sessão

if (isset($_SESSION['cpf'])) { // vericando se o cpf já está na sessão
    header("location:" . WEB . "/professor.php"); //redirecionando para a página do professor
    die(); // encerrando a execução
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Login - SCADAF</title>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <link rel="stylesheet" href="css/generalstyle.css">
        <link rel="stylesheet" href="css/loginstyle.css">
    </head>

    <body class="bg-light">

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" id="logo" href="../index.php">SCADAF</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>





        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 mx-auto">
                    <div class="login-panel panel panel-default">
                        <div class="card col-md mb-5 popup alert-danger" id="popup">
                            <div class="card-body">
                                Professor não cadastrado. Verifique se o CPF foi digitado corretamente!
                            </div>
                        </div>

                        <script type="text/javascript">
                            var parUrl = window.location + "";
                            parUrl = parUrl.substring(parUrl.indexOf("?") + 1);

                            if (parUrl == 1) {
                                document.getElementById('popup').style.display = 'block';
                                window.setInterval('fechar()', 4000);
                            }

                            function fechar() {
                                document.getElementById('popup').style.display = 'none';
                            }
                        </script>


                        <div class="panel-heading">
                            <h3 class="panel-title ">Login</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" method="POST" action="<?= SCRIPTS . "/loginProfessor.php" ?>">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="CPF" name="cpf" type="number" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Senha" name="senha" type="password" value="">
                                    </div>
                                    <input type="submit" class="btn btn-lg btn-success btn-block" value="Login">
                                    <p class="text-center mt-4">Não é cadastrado ainda? <a href="cadastro.php">Clique aqui</a>.</p>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </body>

</html>