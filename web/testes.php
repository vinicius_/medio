<?php

//importação das classes necessárias
require '../dao/AlunoDao.php';


session_start();//iniciando a sessão
$cpf_prof = $_SESSION['cpf']; // aramzenando o cpf do professor em uma variavel local

if (!(isset($cpf_prof))) {//verificando se o professor está logado
    echo '<h1><i>Professor não logado!</i></h1>';
    die();
}
//buscando todos os alunos pertencentes ao professor
$daoA = new AlunoDao();
$alunos = $daoA->allAlunos($cpf_prof); // array
?>

<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Teste</title>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <link rel="stylesheet" href="css/generalstyle.css">
        <link rel="stylesheet" href="css/testesstyle.css">

    </head>

    <body>

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" id="logo" href="../index.php">SCADAF</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="logout.php">Sair</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="wrapper" class="toggled">
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <li class="sidebar-brand">
                        <a href="professor.php">
                            Home
                        </a>
                    </li>
                    <li>
                        <a href="alunos.php" >Alunos</a>
                    </li>
                    <li>
                        <a href="esportes.php">Esportes</a>
                    </li>
                    <li class="bg-secondary">
                        <a href="testes.php" class="text-light">Testes</a>
                    </li>
                </ul>
            </div>

            <div id="page-content-wrapper">
                <h3 class="text-center mb-5">Testes</h3>
                <div style="height: 300px; overflow: auto">
                <p>De acordo com o manual Proesp-Br (GAYA e GAYA, 2016) os testes que medem o nível de desempenho esportivo do indivíduo são:<br/><br/>

<b>Teste de Aptidão Cardiorrespiratória (corrida/caminhada dos 6,9,12  minutos):</b><br/><br/>

Orientação: Dividem-se os alunos em grupos adequados às dimensões da pista. Informa-se aos alunos sobre a execução dos testes dando ênfase ao fato de que devem correr o maior tempo possível, evitando piques de velocidade intercalados por longas caminhadas. Durante o teste, informa-se ao aluno a passagem do tempo 2, 4 e 5. Ao final do teste soará um sinal (apito) sendo que os alunos deverão interromper a corrida, permanecendo no lugar onde estavam (no momento do apito) até ser anotada ou sinalizada a distância percorrida. Anotação: Os resultados serão anotados em metros com uma casa após a vírgula.<br/><br/>

<b>Teste de força explosiva de membros superiores (arremesso do medicineball):</b><br/><br/>

Material: Uma trena e um medicineball de 2 kg ou utilize um saco de areia com 2 kg.<br/><br/>

Orientação: A trena é fixada no solo perpendicularmente à parede. O ponto zero da trena é fixado junto à parede. O aluno senta-se com os joelhos estendidos, as pernas unidas e as costas completamente apoiadas à parede. Segura a medicineball junto ao peito com os cotovelos flexionados. Ao sinal do avaliador o aluno deverá lançar a bola à maior distância possível, mantendo as costas apoiadas na parede. A distância do arremesso será registrada a partir do ponto zero até o local em que a bola tocou ao solo pela primeira vez. Serão realizados dois arremessos, registrando-se para fins de avaliação o melhor resultado. Sugere-se que a medicineball seja banhada em pó branco para facilitar a identificação precisa do local onde tocou pela primeira vez ao solo.<br/><br/>

Anotação: A medida será registrada em centímetros com uma casa após a vírgula.<br/><br/>

<b>Teste de força explosiva de membros inferiores (salto horizontal):</b><br/><br/>

Material: Uma trena e uma linha traçada no solo.<br/><br/>

Orientação: A trena é fixada ao solo, perpendicularmente à linha de partida. A linha de partida pode ser sinalizada com giz, com fita crepe ou ser utilizada uma das linhas que demarcam as quadras esportivas. O ponto zero da trena situa-se sobre a linha de partida. O avaliado coloca-se imediatamente atrás da linha, com os pés paralelos, ligeiramente afastados, joelhos flexionados, tronco ligeiramente projetado à frente. Ao sinal o aluno deverá saltar a maior distância possível aterrissando com os dois pés em simultâneo. Serão realizadas duas tentativas, será considerado para fins de avaliação o melhor resultado.<br/><br/>

Anotação: A distância do salto será registrada em centímetros, com uma casa após a vírgula, a partir da linha traçada no solo até o calcanhar mais próximo desta.<br/><br/>

<b>Teste de agilidade (teste do quadrado):</b><br/><br/>

Material: um cronômetro, um quadrado com 4 metros de lado. Quatro garrafas de refrigerante de 2 litros do tipo PET cheias de areia. Piso antiderrapante.<br/><br/>

Orientação: Demarca-se no local de testes um quadrado de quatro metros de lado. Coloca-se uma garrafa PET em cada ângulo do quadrado. Uma fita crepe ou uma reta desenhada com giz indica a linha de partida (ver figura abaixo). O aluno parte da posição de pé, com um pé avançado à frente imediatamente atrás da linha de partida (em um dos vértices do quadrado). Ao sinal do avaliador, deverá deslocar-se em velocidade máxima e tocar com uma das mãos na garrafa situada no canto em diagonal do quadrado (atravessa o quadrado). Na sequência, corre para tocar à garrafa à sua esquerda (ou direita) e depois se desloca para tocar a garrafa em diagonal (atravessa o quadrado em diagonal). Finalmente, corre em direção à última garrafa, que corresponde ao ponto de partida. O cronômetro deverá ser acionado pelo avaliador no momento em que o avaliado tocar pela primeira vez com o pé o interior do quadrado e será travado quando tocar com uma das mãos na quarta garrafa. Serão realizadas duas tentativas, sendo registrado para fins de avaliação o menor tempo.<br/><br/>

Anotação: A medida será registrada em segundos e centésimos de segundo (duas casas após a vírgula).<br/><br/>

<b>Teste de velocidade de deslocamento (corrida de 20 metros):</b><br/><br/>

Material: Um cronômetro e uma pista de 20 metros demarcada com três linhas paralelas no solo da seguinte forma: a primeira (linha de partida); a segunda, distante 20m da primeira (linha de cronometragem) e a terceira linha, marcada a um metro da segunda (linha de chegada). A terceira linha serve como referência de chegada para o aluno na tentativa de evitar que ele inicie a desaceleração antes de cruzar a linha de cronometragem. Duas garrafas do tipo PET de 2 litros para a sinalização da primeira e terceira linhas.<br/><br/>

Orientação: O estudante parte da posição de pé, com um pé avançado à frente imediatamente atrás da primeira linha (linha de partida) e será informado que deverá cruzar a terceira linha (linha de chegada) o mais rápido possível. Ao sinal do avaliador, o aluno deverá deslocar-se em direção à linha de chegada. O avaliador deverá acionar o cronômetro no momento em que o avaliado, ao dar o primeiro passo, toque o solo pela primeira vez com um dos pés além da linha de partida. O cronômetro será travado quando o aluno ao cruzar a segunda linha (linha de cronometragem) tocar pela primeira vez ao solo.<br/><br/>

Anotação: O cronometista registrará o tempo do percurso em segundos e centésimos de segundos (duas casas após a vírgula).</p>
                </div>
                
                <div class="container">
                    <form method='POST' action='resultado.php' class="mt-5">
                        <div class="form-group m-4">
                            <label class="text-center font-weight-bold" style="display: block;">Esporte</label>
                            <label  for="inpFut" class="card m-2 ml-4 lbForm" style="width: 10em; display: inline-block;"> 
                                <img class="card-img-top" src="img/futebol.png" alt="Card image cap">
                                <div class="card-body bg-secondary">
                                    <input type="radio" name="esporte"  value="futsal" id="inpFut" required>
                                    <h5 class="card-title text-white text-center">Futsal</h5>
                                </div>
                            </label>

                            <label  for="inpVol" class="card m-2 ml-4 lbForm" style="width: 10em; display: inline-block;">
                                <img class="card-img-top" src="img/volei.png" alt="Card image cap">
                                <div class="card-body bg-secondary">
                                    <input type="radio" name="esporte" value="volei" id="inpVol">
                                    <h5 class="card-title text-white text-center">Vôlei</h5>
                                </div>
                            </label>

                            <label for="inpBas" class="card m-2 ml-4 lbForm" style="width: 10em; display: inline-block;">
                                <img class="card-img-top" src="img/basquete.png" alt="Card image cap">
                                <div class="card-body bg-secondary">
                                    <input class="text-center" type="radio" name="esporte" value="basquete"  id="inpBas">
                                    <h5 class="card-title text-white text-center" style="display: inline;">Basquete</h5>
                                </div>
                            </label>


                            <label for="inpAt" class="card m-2 ml-4 lbForm" style="width: 10em; display: inline-block;">
                                <img class="card-img-top" src="img/atletismo.png" alt="Card image cap">
                                <div class="card-body bg-secondary">
                                    <input type="radio" name="esporte" value="atletismo" id="inpAt">
                                    <h5 class="card-title text-white text-center">Atletismo</h5>
                                </div>
                            </label>
                            
                            <label for="inpHand" class="card m-2 ml-4 lbForm" style="width: 10em; display: inline-block;">
                                <img class="card-img-top" src="img/handebol.png" alt="Card image cap">
                                <div class="card-body bg-secondary">
                                    <input type="radio" name="esporte" value="handebol" id="inpHand">
                                    <h5 class="card-title text-white text-center">Handebol</h5>
                                </div>
                            </label>
                        </div>
                </div>

                <div class="form-group col-xg h-5 mt-5" >
                    <label class="text-center font-weight-bold" for="exampleFormControlSelect2" style="display: block;">Nome do Aluno</label>
                    <select multiple class="form-control" name="idAluno" id="selectNome" style="height: 300px" required>
                        <?php
                        for ($i = 0; $i < count($alunos); $i++) {
                            echo "<option value='".$alunos[$i]['idAluno']."'>" . $alunos[$i]['nome'] . "</option>";
                        }
                        ?>
                    </select>
                </div>

                <input class="btn btn-primary btn-block mt-5 mb-5" value="Verificar" type="submit" style="height: 50px">
                </form>
            </div>
        </div>
    </body>

</html>
