<?php
//importação das classes necessárias
include '../include/Config.php';
?>
<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="description" content="">

        <meta name="author" content="">

        <title>Cadastro - SCADAF</title>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <link rel="stylesheet" href="css/generalstyle.css">
        <link rel="stylesheet" href="css/cadastrostyle.css">
    </head>

    <body class="bg-light">

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" id="logo" href="../index.php">SCADAF</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>

        <div class="container mt-4 col-sm-8" >
             <form id="cad" method="POST" action="<?= SCRIPTS."/CadastroProfessor.php"?>">
                <fieldset>
                    <div class="form-group">
                        <div class="form-label-group">
                            <label for="inputNome">Nome</label>
                            <input type="text" name="nome" id="inputNome" class="form-control" placeholder="Nome Completo" required="required" autofocus="autofocus">                    
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-label-group">
                            <label for="inputCPF">CPF</label>
                            <input type="number" name="cpf" id="inputCPF" class="form-control" placeholder="CPF" required="required" autofocus="autofocus">                    
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-label-group">
                            <label for="inputTelefone">Telefone</label>
                            <input type="number" name="telefone" id="inputTelefone" class="form-control" placeholder="Telefone" required="required" autofocus="autofocus">                    
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-label-group">
                            <label for="inputEmail">Email</label>
                            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Endereço de email" required="required" autofocus="autofocus">                    
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-label-group">
                            <label for="inputSenha">Senha</label>
                            <input type="password" name="senha" id="inputSenha" class="form-control" placeholder="Senha" required="required">
                        </div>
                    </div>

                    <input class="btn btn-primary btn-block" value="Cadastrar" type="submit">
                </fieldset>
            </form>
        </div>
    </body> 
</html>