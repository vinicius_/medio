<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Esportes</title>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <link rel="stylesheet" href="css/generalstyle.css">
        <link rel="stylesheet" href="css/esportesstyle.css">

    </head>

    <body>

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" id="logo" href="../index.php">SCADAF</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="logout.php">Sair</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="wrapper" class="toggled ">
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <li class="sidebar-brand">
                        <a href="professor.php">
                            Home
                        </a>
                    </li>
                    <li>
                        <a href="alunos.php">Alunos</a>
                    </li>
                    <li class="bg-secondary">
                        <a href="#" class="text-light">Esportes</a>
                    </li>
                    <li>
                        <a href="testes.php">Testes</a>
                    </li>
                </ul>
            </div>

            <div id="page-content-wrapper">
                    <div class="card ml-5" style="width: 10em;">
                    <img class="card-img-top" src="img/futebol.png" alt="Card image cap">
                        <a href="futsal.php" class="btn btn-light card-body" style="width: 100%">Ir para</a>
                </div>
                
                <div class="card" style="width: 10em;">
                    <img class="card-img-top" src="img/volei.png" alt="Card image cap">
                        <a href="volei.php" class="btn btn-light card-body" style="width: 100%">Ir para</a>
                </div>
                
                <div class="card" style="width: 10em;">
                    <img class="card-img-top" src="img/basquete.png" alt="Card image cap">
                        <a href="basquete.php" class="btn btn-light card-body" style="width: 100%">Ir para</a>
                </div>
                
                <div class="card" style="width: 10em;">
                    <img class="card-img-top" src="img/atletismo.png" alt="Card image cap">
                        <a href="atletismo.php" class="btn btn-light card-body" style="width: 100%">Ir para</a>
                </div>
                
                <div class="card" style="width: 10em;">
                    <img class="card-img-top" src="img/handebol.png" alt="Card image cap">
                    <a href="handebol.php" class="btn btn-light card-body" style="width: 100%">Ir para</a>
                </div>
            </div>

        </div>
    </body>

</html>