<?php
//importação das classes necessárias
require_once '../dao/AlunoDao.php';
require_once '../model/Classificacao.php';
require_once '../model/Esporte.php';
session_start();//iniciando a sessão
$cpf_prof = $_SESSION['cpf'];// aramzenando o cpf do professor em uma variavel local

$post = filter_input_array(INPUT_POST, FILTER_DEFAULT);//filtrando os dados do POST para uma variável local

if(!isset($cpf_prof)){ //verifica se o professor está logado
    echo "<h3>Professor nao logado (web/fusal)</h3>";
    die();
}
?>
<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Atletismo</title>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <link rel="stylesheet" href="css/generalstyle.css">
        <link rel="stylesheet" href="css/professorstyle.css">

    </head>

    <body>

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" id="logo" href="../index.php">SCADAF</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="logout.php">Sair</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                
                <div class="col  m-4">
                    <h4 class="text-center pb-5">Lista dos alunos aptos</h4>

                    <table class="table">
                        <tbody>
<!--                            <tr>
                                <td scope="row">Maria</td>
                            </tr>
                            <tr>
                                <td scope="row">João</td>
                            </tr>-->
                            <?php
                            $daoA = new AlunoDao();
                            $todosAlunos = $daoA->allAlunos($cpf_prof);
                            $faz = 0;
                            $total = count($todosAlunos);

                            for ($i = 0; $i < $total; $i++) {

                                $aluno = $daoA->objAluno($daoA->getAlunoFull($cpf_prof, $todosAlunos[$i]['idAluno']));
                                $classi = new Classificacao($aluno);
                                $esporte = new Esporte($classi, 1);

                                if ($esporte->atletismo) {
                                    echo "<tr><td scope='row'>" . $aluno->getNome() . "</td> </tr>";
                                    $faz++;
                                }
                            }

                            $fazz = (int) ($faz * 100) / $total;
                            $resto = 100 - $fazz;
                            if ($faz == 0) {
                                $resto = 100;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="container">
            <h4 class="m-3 mb-5 text-center">Análise Geral</h4>
            <label>Média de alunos aptos e não-aptos</label>
            <div class="progress mb-4" style="height: 30px;">
                <div class="progress-bar" role="progressbar" style="width: <?= $fazz ?>%" aria-valuenow="<?= $fazz ?>%" aria-valuemin="0" aria-valuemax="100">Alunos aptos</div>
                <div class="progress-bar bg-success" role="progressbar" style="width: <?= $resto ?>%" aria-valuenow="<?= $resto ?>%" aria-valuemin="0" aria-valuemax="100">Alunos não-aptos</div>
            </div>

            <label>Alunos que praticam o esporte</label>
            <div class="progress mb-5" style="height: 30px;">
                <div class="progress-bar" role="progressbar" style="width: <?= $fazz ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
            </div>
        </div>
    </body>

</html>