<?php
//importação das classes necessárias
include '../include/Config.php';
session_start();//iniciando a sessão

if (!(isset($_SESSION['cpf']) and isset($_SESSION['professor']))) {//verificando se o professor está logado
    echo "Professor não logado";
    die();
}
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="description" content="">

        <meta name="author" content="">

        <title>Cadastro - SCADAF</title>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <link rel="stylesheet" href="css/generalstyle.css">
        <link rel="stylesheet" href="css/cadastrostyle.css">

    </head>
    <body class="bg-light">

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" id="logo" href="../index.php">SCADAF</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>

        <div class="container mt-4 col-sm-8" >
            <div class="card col-md mb-5 popup alert-success" id="popup">
                <div class="card-body">
                    Aluno Cadastrado com sucesso!
                </div>
            </div>
            
            <div class="card col-md mb-5 popup alert-warning" id="popup2">
                <div class="card-body">
                    Aluno já cadastrado!   
                </div>
            </div>

            <script type="text/javascript">
                var parUrl = window.location + "";
                parUrl = parUrl.substring(parUrl.indexOf("?") + 1);

                if (parUrl == 1) {
                    document.getElementById('popup').style.display = 'block';
                    window.setInterval('fechar()', 4000);
                }
                
                if (parUrl == 2) {
                    document.getElementById('popup2').style.display = 'block';
                    window.setInterval('fechar2()', 4000);
                }

                function fechar() {
                    document.getElementById('popup').style.display = 'none';
                }
                
                function fechar2() {
                    document.getElementById('popup2').style.display = 'none';
                }
                
            </script>
            <h3>Cadastro de Aluno</h3>
            <label class="mb-5">Preencha todos os campos corretamente.</label>
            <form method="POST" action="<?= SCRIPTS . '/cadastroAluno.php' ?>">
                <div class="jumbotron">
                    <fieldset>
                        <legend style="text-align:center">Dados Pessoais</legend>

                        <div class="form-group">
                            <div class="form-label-group">
                                <label for="inputNome">Nome</label>
                                <input type="text" name="nome" id="inputNome" class="form-control" placeholder="Ex.: Ana Maria Sousa" required="required" autofocus="autofocus">                    
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-label-group">
                                <label for="inputIdade">Data de Nasc</label>
                                <input type="date" name="idade" id="inputIdade" class="form-control" placeholder="Ex.: 17" required="required" autofocus="autofocus">                    
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputSexo">Sexo</label>
                            <select class="form-control" id="inputSexo" name="sexo" required>
                                <option value="" disabled selected>Selecione...</option>
                                <option value="m">Masculino</option>
                                <option value="f">Feminino</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="inputGrupo">Série</label>
                            <select class="form-control" id="inputGrupo" name="grupo" required>
                                <option value="" disabled selected>Selecione...</option>
                                <option value="1">1º Ano Biotecnologia</option>
                                <option value="2">2º Ano Biotecnologia</option>
                                <option value="3">3º Ano Biotecnologia</option>
                                <option value="4">1º Ano Informática</option>
                                <option value="5">2º Ano Informática</option>
                                <option value="6">3º Ano Informática</option>
                            </select>
                        </div>
                    </fieldset>
                </div>
                
                <div class="jumbotron">   
                    <fieldset>
                        <legend>Aptidão Física</legend>
                        <div class="form-group">
                            <div class="form-label-group">
                                <label for="inputVelocidade">Velocidade</label>
                                <input type="number" step="any" name="velocidade" id="inputVelocidade" class="form-control" placeholder="Ex.: 3.54" required="required" autofocus="autofocus">                    
                            </div>
                        </div>  

                        <div class="form-group">
                            <div class="form-label-group">
                                <label for="inputSalto">Salto em Distância</label>
                                <input type="number" step="any" name="salto_distancia" id="inputSalto" class="form-control" placeholder="Ex.: 180.68" required="required" autofocus="autofocus">                    
                            </div>
                        </div>  

                        <div class="form-group">
                            <div class="form-label-group">
                                <label for="inputArremesso">Arremesso de Medicine Ball</label>
                                <input type="number" step="any" name="arremesso_ball" id="inputArremesso" class="form-control" placeholder="Ex.: 2.41" required="required" autofocus="autofocus">                    
                            </div>
                        </div> 

                        <div class="form-group">
                            <div class="form-label-group">
                                <label for="inputAgilidade">Agilidade</label>
                                <input type="number" step="any" name="agilidade" id="inputAgilidade" class="form-control" placeholder="Ex.: 5.55" required="required" autofocus="autofocus">                    
                            </div>
                        </div> 

                        <div class="form-group">
                            <div class="form-label-group">
                                <label for="inputResisCard">Resistência Cardiovascular</label>
                                <input type="number" step="any" name="resistencia_cardiovascular" id="inputResisCard" class="form-control" placeholder="Ex.: 2520" required="required" autofocus="autofocus">                    
                            </div>
                        </div>                    
                    </fieldset>
                </div>
                
                <div class="jumbotron">
                    <fieldset>
                        <legend>Dados de Saúde</legend>

                        <div class="form-group">
                            <div class="form-label-group">
                                <label for="inputAltura">Altura</label>
                                <input type="number" step="any" name="altura" id="inputAltura" class="form-control" placeholder="Ex.: 1.65" autofocus="autofocus">                    
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-label-group">
                                <label for="inputPeso">Peso</label>
                                <input type="number" step="any" name="peso" id="inputPeso" class="form-control" placeholder="Ex.: 75.0"  autofocus="autofocus">                    
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-label-group">
                                <label for="inputCintura">Cintura</label>
                                <input type="number" step="any" name="cintura" id="inputIdade" class="form-control" placeholder="Ex.: 89"  autofocus="autofocus">                    
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-label-group">
                                <label for="inputQuadril">Quadril</label>
                                <input type="number" step="any" name="quadril" id="inputQuadril" class="form-control" placeholder="Ex.: 100"  autofocus="autofocus">                    
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-label-group">
                                <label for="inputEnvergadura">Envergadura</label>
                                <input type="number" step="any" name="envergadura" id="inputEnvergadura" class="form-control" placeholder="Ex.: 1.70" autofocus="autofocus">                    
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-label-group">
                                <label for="inputFlex">Flexibilidade</label>
                                <input type="number" step="any" name="flexibilidade" id="inputFlex" class="form-control" placeholder="Ex.: 50.26"  autofocus="autofocus">                    
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-label-group">
                                <label for="inputResisAb">Resistência Abdominal</label>
                                <input type="number" step="any" name="resistencia_abdominal" id="inputResisAb" class="form-control" placeholder="Ex.: 137"  autofocus="autofocus">                    
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-label-group">
                                <label>Resistência Cardiovascular</label><br/>
                                <label id="cardio"></label>                    
                            </div>
                        </div>
                    </fieldset>
                </div>    

                <script>
                    var inputModelo = document.getElementById('inputResisCard');

                    var labelRepete = document.getElementById('cardio');

                    inputModelo.addEventListener('keyup', function () {
                        labelRepete.innerHTML = inputModelo.value;
                    });
                </script>
                    
                <div class="jumbotron">
                    <fieldset>
                        <legend>Dobras Cutâneas</legend>

                        <div class="form-group">
                            <div class="form-label-group">
                                <label for="inputTriceps">Tríceps</label>
                                <input type="number" step="any" name="triceps" id="inputTriceps" class="form-control" placeholder="Ex.: 0.59" required="required" autofocus="autofocus">                    
                            </div>
                        </div>  

                        <div class="form-group">
                            <div class="form-label-group">
                                <label for="inputSubcapsular">Subscapular</label>
                                <input type="number" step="any" name="subscapular" id="inputSubcapsular" class="form-control" placeholder="Ex.: 0.41" required="required" autofocus="autofocus">                    
                            </div>
                        </div> 
                    </fieldset>
                </div>    
                    
                <input id="sub" class="btn btn-primary btn-block mt-5 mb-5" value="Cadastrar" type="submit">
            </form>
        </div>
    </body>
</html>
