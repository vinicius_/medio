<?php
//importação das classes necessárias
require '../dao/AlunoDao.php';
session_start();//iniciando a sessão
$cpf_prof = $_SESSION['cpf']; // aramzenando o cpf do professor em uma variavel local

if (!(isset($cpf_prof))) { //verificando se o professor está logado
    echo '<h1><i>Professor não logado!</i></h1>';
    die();
}

$daoA = new AlunoDao(); //objeto AlunoDAO
$alunos = $daoA->allAlunos($cpf_prof); // array de todos os alunos do professor
?>
<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Alunos - Professor</title>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <link rel="stylesheet" href="css/generalstyle.css">
        <link rel="stylesheet" href="css/alunosstyle.css">

    </head>

    <body>

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" id="logo" href="../index.php">SCADAF</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="logout.php">Sair</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="wrapper" class="toggled">
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <li class="sidebar-brand">
                        <a href="professor.php">
                            Home
                        </a>
                    </li>
                    <li class="bg-secondary">
                        <a href="alunos.php" class="text-light">Alunos</a>
                    </li>
                    <li>
                        <a href="esportes.php">Esportes</a>
                    </li>
                    <li>
                        <a href="testes.php">Testes</a>
                    </li>
                </ul>
            </div>

            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <h2>Alunos</h2>

                    <form method='GET' action='alunos.php'>
                            <div class="form-group mt-5">
                                <label for="inputGrupo">Filtro de busca:</label>
                                <select class="form-control" id="idGrupo" name="idGrupo">
                                    <option value="" selected>Todos</option>
                                    <option value="1">1º Ano Biotecnologia</option>
                                    <option value="2">2º Ano Biotecnologia</option>
                                    <option value="3">3º Ano Biotecnologia</option>
                                    <option value="4">1º Ano Informática</option>
                                    <option value="5">2º Ano Informática</option>
                                    <option value="6">3º Ano Informática</option>
                                </select>
                            </div>
                            <input id="sub" style="margin:0 auto" class="btn btn-primary btn-block mb-4 col-sm-5" value="Listar" type="submit">
                    </form>

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Idade</th>
                                <th>Grupo</th>
                                <th>Sexo</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            function idade($nasc) {
                                $es = explode("-", $nasc);
                                $birthDate = array($es[2],$es[1],$es[0]);
                                //get age from date or birthdate
                                $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y") - $birthDate[2]) - 1) : (date("Y") - $birthDate[2]));
                                return $age;
                            }

                            function sexo($s){
                                if($s == 'm'){
                                    return "Masculino";
                                }
                                return "Feminino";
                            }

                            if(isset($_GET['idGrupo'])) {
                                $idG = $_GET['idGrupo'];
                            } else {
                                $idG = -1;
                            }

                            switch ($idG) {
                                case 1:
                                    $filtro = $daoA->filtroPorGrupo($cpf_prof, 1);
                                    for ($i = 0; $i < count($filtro); $i++) {
                                        echo "<form method='POST' action='aluno.php' >";
                                        echo "<tr>"
                                        . "<td>" . $filtro[$i]['nome'] . "</td>"
                                        . "<td>" . (idade($filtro[$i]['dataNasc'])) . "</td>"
                                        //. "<td>" . $alunos[$i]['cpf_aluno'] . "</td>"
                                        . "<td>" . utf8_encode($filtro[$i]['nomeGrupo']) . "</td>"
                                        . "<td>" . ( sexo($filtro[$i]['sexo'])) . "</td>";
                                        echo "<input type='hidden' name='idaluno' value='" . $filtro[$i]['idAluno'] . "'>";
                                        echo "<td><input type='submit' class='btn btn-primary btn-md' value='Ver Perfil'></td>";
                                        echo "</tr>";
                                        echo "</form>";
                                    }
                                    break;
                                case 2:
                                    $filtro = $daoA->filtroPorGrupo($cpf_prof, 2);
                                    for ($i = 0; $i < count($filtro); $i++) {
                                        echo "<form method='POST' action='aluno.php' >";
                                        echo "<tr>"
                                        . "<td>" . $filtro[$i]['nome'] . "</td>"
                                        . "<td>" . (idade($filtro[$i]['dataNasc'])) . "</td>"
                                        //. "<td>" . $alunos[$i]['cpf_aluno'] . "</td>"
                                        . "<td>" . utf8_encode($filtro[$i]['nomeGrupo']) . "</td>"
                                        . "<td>" . ( sexo($filtro[$i]['sexo'])) . "</td>";
                                        echo "<input type='hidden' name='idaluno' value='" . $filtro[$i]['idAluno'] . "'>";
                                        echo "<td><input type='submit' class='btn btn-primary btn-md' value='Ver Perfil'></td>";
                                        echo "</tr>";
                                        echo "</form>";
                                    }                                    
                                    break;
                                case 3:
                                    $filtro = $daoA->filtroPorGrupo($cpf_prof, 3);
                                    for ($i = 0; $i < count($filtro); $i++) {
                                        echo "<form method='POST' action='aluno.php' >";
                                        echo "<tr>"
                                        . "<td>" . $filtro[$i]['nome'] . "</td>"
                                        . "<td>" . (idade($filtro[$i]['dataNasc'])) . "</td>"
                                        //. "<td>" . $alunos[$i]['cpf_aluno'] . "</td>"
                                        . "<td>" . utf8_encode($filtro[$i]['nomeGrupo']) . "</td>"
                                        . "<td>" . ( sexo($filtro[$i]['sexo'])) . "</td>";
                                        echo "<input type='hidden' name='idaluno' value='" . $filtro[$i]['idAluno'] . "'>";
                                        echo "<td><input type='submit' class='btn btn-primary btn-md' value='Ver Perfil'></td>";
                                        echo "</tr>";
                                        echo "</form>";
                                    }
                                    break;
                                case 4:
                                    $filtro = $daoA->filtroPorGrupo($cpf_prof, 4);
                                    for ($i = 0; $i < count($filtro); $i++) {
                                        echo "<form method='POST' action='aluno.php' >";
                                        echo "<tr>"
                                        . "<td>" . $filtro[$i]['nome'] . "</td>"
                                        . "<td>" . (idade($filtro[$i]['dataNasc'])) . "</td>"
                                        //. "<td>" . $alunos[$i]['cpf_aluno'] . "</td>"
                                        . "<td>" . utf8_encode($filtro[$i]['nomeGrupo']) . "</td>"
                                        . "<td>" . ( sexo($filtro[$i]['sexo'])) . "</td>";
                                        echo "<input type='hidden' name='idaluno' value='" . $filtro[$i]['idAluno'] . "'>";
                                        echo "<td><input type='submit' class='btn btn-primary btn-md' value='Ver Perfil'></td>";
                                        echo "</tr>";
                                        echo "</form>";
                                    }
                                    break;
                                case 5:
                                    $filtro = $daoA->filtroPorGrupo($cpf_prof, 5);
                                    for ($i = 0; $i < count($filtro); $i++) {
                                        echo "<form method='POST' action='aluno.php' >";
                                        echo "<tr>"
                                        . "<td>" . $filtro[$i]['nome'] . "</td>"
                                        . "<td>" . (idade($filtro[$i]['dataNasc'])) . "</td>"
                                        //. "<td>" . $alunos[$i]['cpf_aluno'] . "</td>"
                                        . "<td>" . utf8_encode($filtro[$i]['nomeGrupo']) . "</td>"
                                        . "<td>" . ( sexo($filtro[$i]['sexo'])) . "</td>";
                                        echo "<input type='hidden' name='idaluno' value='" . $filtro[$i]['idAluno'] . "'>";
                                        echo "<td><input type='submit' class='btn btn-primary btn-md' value='Ver Perfil'></td>";
                                        echo "</tr>";
                                        echo "</form>";
                                    }
                                    break;
                                case 6:
                                    $filtro = $daoA->filtroPorGrupo($cpf_prof, 6);
                                    for ($i = 0; $i < count($filtro); $i++) {
                                        echo "<form method='POST' action='aluno.php' >";
                                        echo "<tr>"
                                        . "<td>" . $filtro[$i]['nome'] . "</td>"
                                        . "<td>" . (idade($filtro[$i]['dataNasc'])) . "</td>"
                                        //. "<td>" . $alunos[$i]['cpf_aluno'] . "</td>"
                                        . "<td>" . utf8_encode($filtro[$i]['nomeGrupo']) . "</td>"
                                        . "<td>" . ( sexo($filtro[$i]['sexo'])) . "</td>";
                                        echo "<input type='hidden' name='idaluno' value='" . $filtro[$i]['idAluno'] . "'>";
                                        echo "<td><input type='submit' class='btn btn-primary btn-md' value='Ver Perfil'></td>";
                                        echo "</tr>";
                                        echo "</form>";
                                    }
                                    break;
                                default:
                                    for ($i = 0; $i < count($alunos); $i++) {
                                        echo "<form method='POST' action='aluno.php' >";
                                        echo "<tr>"
                                        . "<td>" . $alunos[$i]['nome'] . "</td>"
                                        . "<td>" . (idade($alunos[$i]['dataNasc'])) . "</td>"
                                        //. "<td>" . $alunos[$i]['cpf_aluno'] . "</td>"
                                        . "<td>" . utf8_encode($alunos[$i]['nomeGrupo']) . "</td>"
                                        . "<td>" . ( sexo($alunos[$i]['sexo'])) . "</td>";
                                        echo "<input type='hidden' name='idaluno' value='" . $alunos[$i]['idAluno'] . "'>";
                                        echo "<td><input type='submit' class='btn btn-primary btn-md' value='Ver Perfil'></td>";
                                        echo "</tr>";
                                        echo "</form>";
                                    }
                            }
                            
                            
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
    </body>

</html>

