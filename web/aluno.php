<?php
//importação das classes necessárias
require_once '../dao/Conexao.php';
require_once '../dao/AlunoDao.php';
require_once '../model/Classificacao.php';
require_once '../model/Esporte.php';
require_once '../scripts/scripts.php';

session_start(); //iniciando a sessão


$post = filter_input_array(INPUT_POST, FILTER_DEFAULT); //filtrando os dados do POST para uma variável local
$cpf_prof = $_SESSION['cpf']; // aramzenando o cpf do professor em uma variavel local

if (!isset($post['idaluno']) or ! isset($cpf_prof)) { // verficiando se o cpf do prodessor e o do aluno já estão configurados
    echo "Aluno ou professor nao setado, aluno.php";
    die();
}

$daoA = new AlunoDao(); //objeto do tipo aluno dao
$aluno = $daoA->objAluno($daoA->getAlunoFull($cpf_prof, $post['idaluno'])); //obj com todos os dados do alunno
$classi = new Classificacao($aluno); //definindo o resultado do aluno com base nos valores dele atribuidos

$esporte = new Esporte($classi, 1); // classificando o aluno nos esportes
?>
<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Aluno</title>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <link rel="stylesheet" href="css/generalstyle.css">
        <link rel="stylesheet" href="css/professorstyle.css">

    </head>

    <body>

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" id="logo" href="../index.php">SCADAF</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="logout.php">Sair</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container-fluid mx-auto col-sm-5" align="center">
            <table class="table">
                <div class="container p-5 font-weight-light">
                    <h3>Aluno: <?= $aluno->nome ?></h3>
                </div>

                <tbody>
                    <tr>
                        <td scope="row">Altura</td>
                        <td><?= $aluno->altura ?></td>
                    </tr>
                    <tr>
                        <td scope="row">Peso</td>
                        <td><?= $aluno->peso ?></td>
                    </tr>
                    <tr>
                        <td scope="row">Idade</td>
                        <td><?= $aluno->idade ?></td>
                    </tr>
                    <tr>
                        <td scope="row">IMC</td>
                        <td>
                            <?php 
                                if($aluno->altura != 0 && $aluno->peso != 0) {
                                    echo round(($aluno->peso)/(pow($aluno->altura, 2)), 2);
                                } else {
                                    echo "0";
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row">Cintura</td>
                        <td><?= $aluno->cintura ?></td>
                    </tr>
                    <tr>
                        <td scope="row">Quadril</td>
                        <td><?= $aluno->quadril ?></td>
                    </tr>
                    <tr>
                        <td scope="row">Envergadura</td>
                        <td><?= $aluno->envergadura ?></td>
                    </tr>
                    <tr>
                        <td scope="row">Flexibilidade</td>
                        <td><?= $aluno->flexibilidade ?></td>
                    </tr>
                    <tr>
                        <td scope="row">Resistência Abdominal</td>
                        <td><?= $aluno->resistencia_abdominal ?></td>
                    </tr>
                    <tr>
                        <td scope="row">Velocidade</td>
                        <td><?= $aluno->velocidade ?></td>
                    </tr>
                    <tr>
                        <td scope="row">Salto em distância</td>
                        <td><?= $aluno->salto_em_discancia ?></td>
                    </tr>
                    <tr>
                        <td scope="row">Triceps</td>
                        <td><?= $aluno->triceps ?></td>
                    </tr>
                    <tr>
                        <td scope="row">Subcapsular</td>
                        <td><?= $aluno->subscapular ?></td>
                    </tr>
                    <tr>
                        <td scope="row">Resistência Cardiovascular</td>
                        <td><?= $aluno->resistencia_cardiovascular ?></td>
                    </tr>
                    <tr>
                        <td scope="row">Arremesso de Medicine Ball</td>
                        <td><?= $aluno->arremesso_de_medicine_ball ?></td>
                    </tr>
                    <tr>
                        <td scope="row">Agilidade</td>
                        <td><?= $aluno->agilidade ?></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="container-fluid mt-5" align="center">
            <h4 >Aptidões</h4>
        </div>

        <div class="container-fluid mx-auto col-sm-5" align="center">
            <table class="table">
                <tbody class="text-center">
                <?php 
                
                if($esporte->futebol == true){
                    echo '<tr>
                    <td>
                        <b>Futsal</b>

                        <div class="container-fluid">
                            <div class="panel mt-2">
                                <label>Flexibilidade</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getFlexibilidade()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getFlexibilidade()).'%">
                                        '.$classi->getFlexibilidade().'
                                    </div>
                                </div>
                                
                                <label>Agilidade</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getAgilidade()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getAgilidade()).'%">
                                        '.$classi->getAgilidade().'
                                    </div>
                                </div>
                                
                                <label>Resistência Anaeróbica</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getResistencia_anaerobica()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getResistencia_anaerobica()).'%">
                                        '.$classi->getResistencia_anaerobica().'
                                    </div>
                                </div>
                                
                                <label>Força Explosiva (Membros Superiores)</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getArremesso_ball()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getArremesso_ball()).'%">
                                        '.$classi->getArremesso_ball().'
                                    </div>
                                </div>
                                
                                <label>Velocidade</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getCorrida()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getCorrida()).'%">
                                        '.$classi->getCorrida().'
                                    </div>
                                </div>
                                
                                <label>Resistência Muscular Localizada</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getResistencia_ab()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getResistencia_ab()).'%">
                                        '.$classi->getResistencia_ab().'
                                    </div>
                                </div>
                                
                                <label>Força Explosiva (Membros Inferiores)</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getSalto()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getSalto()).'%">
                                        '.$classi->getSalto().'
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>';
                }
                
                if($esporte->atletismo == true){
                    echo '<tr>
                    <td>
                        <b>Atletismo</b>

                        <div class="container-fluid">
                            <div class="panel mt-2">
                                <label>Flexibilidade</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getFlexibilidade()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getFlexibilidade()).'%">
                                        '.$classi->getFlexibilidade().'
                                    </div>
                                </div>
                                
                                <label>Agilidade</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getAgilidade()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getAgilidade()).'%">
                                        '.$classi->getAgilidade().'
                                    </div>
                                </div>
                                
                                <label>Resistência Anaeróbica</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getResistencia_anaerobica()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getResistencia_anaerobica()).'%">
                                        '.$classi->getResistencia_anaerobica().'
                                    </div>
                                </div>
                                
                                <label>Força Explosiva (Membros Superiores)</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getArremesso_ball()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getArremesso_ball()).'%">
                                        '.$classi->getArremesso_ball().'
                                    </div>
                                </div>
                                
                                <label>Velocidade</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getCorrida()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getCorrida()).'%">
                                        '.$classi->getCorrida().'
                                    </div>
                                </div>
                                
                                <label>Resistência Muscular Localizada</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getResistencia_ab()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getResistencia_ab()).'%">
                                        '.$classi->getResistencia_ab().'
                                    </div>
                                </div>
                                
                                <label>Força Explosiva (Membros Inferiores)</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getSalto()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getSalto()).'%">
                                        '.$classi->getSalto().'
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>';
                }
                
                if($esporte->basquete == true){
                    echo '<tr>
                    <td>
                        <b>Basquete</b>

                        <div class="container-fluid">
                            <div class="panel mt-2">
                                <label>Flexibilidade</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getFlexibilidade()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getFlexibilidade()).'%">
                                        '.$classi->getFlexibilidade().'
                                    </div>
                                </div>
                                
                                <label>Agilidade</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getAgilidade()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getAgilidade()).'%">
                                        '.$classi->getAgilidade().'
                                    </div>
                                </div>
                                
                                <label>Resistência Anaeróbica</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getResistencia_anaerobica()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getResistencia_anaerobica()).'%">
                                        '.$classi->getResistencia_anaerobica().'
                                    </div>
                                </div>
                                
                                <label>Força Explosiva (Membros Superiores)</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getArremesso_ball()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getArremesso_ball()).'%">
                                        '.$classi->getArremesso_ball().'
                                    </div>
                                </div>
                                
                                <label>Velocidade</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getCorrida()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getCorrida()).'%">
                                        '.$classi->getCorrida().'
                                    </div>
                                </div>
                                
                                <label>Resistência Muscular Localizada</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getResistencia_ab()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getResistencia_ab()).'%">
                                        '.$classi->getResistencia_ab().'
                                    </div>
                                </div>
                                
                                <label>Força Explosiva (Membros Inferiores)</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getSalto()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getSalto()).'%">
                                        '.$classi->getSalto().'
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>';
                }
                
                if($esporte->volei == true){
                    echo '<tr>
                    <td>
                        <b>Vôlei</b>

                        <div class="container-fluid">
                            <div class="panel mt-2">
                                <label>Flexibilidade</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getFlexibilidade()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getFlexibilidade()).'%">
                                        '.$classi->getFlexibilidade().'
                                    </div>
                                </div>
                                
                                <label>Agilidade</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getAgilidade()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getAgilidade()).'%">
                                        '.$classi->getAgilidade().'
                                    </div>
                                </div>
                                
                                <label>Resistência Anaeróbica</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getResistencia_anaerobica()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getResistencia_anaerobica()).'%">
                                        '.$classi->getResistencia_anaerobica().'
                                    </div>
                                </div>
                                
                                <label>Força Explosiva (Membros Superiores)</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getArremesso_ball()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getArremesso_ball()).'%">
                                        '.$classi->getArremesso_ball().'
                                    </div>
                                </div>
                                
                                <label>Velocidade</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getCorrida()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getCorrida()).'%">
                                        '.$classi->getCorrida().'
                                    </div>
                                </div>
                                
                                <label>Resistência Muscular Localizada</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getResistencia_ab()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getResistencia_ab()).'%">
                                        '.$classi->getResistencia_ab().'
                                    </div>
                                </div>
                                
                                <label>Força Explosiva (Membros Inferiores)</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getSalto()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getSalto()).'%">
                                        '.$classi->getSalto().'
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>';
                }
                
                if($esporte->handbol == true){
                    echo '<tr>
                    <td>
                        <b>Handebol</b>

                        <div class="container-fluid">
                            <div class="panel mt-2">
                                <label>Flexibilidade</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getFlexibilidade()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getFlexibilidade()).'%">
                                        '.$classi->getFlexibilidade().'
                                    </div>
                                </div>
                                
                                <label>Agilidade</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getAgilidade()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getAgilidade()).'%">
                                        '.$classi->getAgilidade().'
                                    </div>
                                </div>
                                
                                <label>Resistência Anaeróbica</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getResistencia_anaerobica()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getResistencia_anaerobica()).'%">
                                        '.$classi->getResistencia_anaerobica().'
                                    </div>
                                </div>
                                
                                <label>Força Explosiva (Membros Superiores)</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getArremesso_ball()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getArremesso_ball()).'%">
                                        '.$classi->getArremesso_ball().'
                                    </div>
                                </div>
                                
                                <label>Velocidade</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getCorrida()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getCorrida()).'%">
                                        '.$classi->getCorrida().'
                                    </div>
                                </div>
                                
                                <label>Resistência Muscular Localizada</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getResistencia_ab()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getResistencia_ab()).'%">
                                        '.$classi->getResistencia_ab().'
                                    </div>
                                </div>
                                
                                <label>Força Explosiva (Membros Inferiores)</label>
                                <div class="progress mb-3">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="'.nivel($classi->getSalto()).'"
                                         aria-valuemin="0" aria-valuemax="100" style="width:'. nivel($classi->getSalto()).'%">
                                        '.$classi->getSalto().'
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>';
                }
                ?>
                </tbody>
            </table>
        </div>


    </div>
</body>

</html>
